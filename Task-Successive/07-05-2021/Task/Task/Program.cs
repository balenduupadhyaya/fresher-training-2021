﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class Program
{
    public static void Main(string[] args)
    {
        Console.WriteLine("Area of rectangle is " + area.rectangle(2, 5));
        Console.WriteLine("Area of Square is " + area.square(5));
        Console.WriteLine("Area of Circle is " + area.circle(8));
    }
}

static class area
{
    public static float pi = 3.14f;
    public static int rectangle(int l, int b) { return l * b; }
    public static int square(int s) { return s * s; }

    public static int circle(int r) { return (int)3.14 * r * r; }

}
