﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figures
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {

                Console.WriteLine("Choose one from choices : \n1 - Sphere \n2- Cylinder \n3- Cube");
                Console.WriteLine();
                int a = Convert.ToInt32(Console.ReadLine());


                switch (a)
                {
                    case 1:
                        Shapes S = new Sphere();
                        {
                            Console.WriteLine();
                            Console.WriteLine("Choose one from choices : \n1 - Surface Ara \n2- Volume");
                            Console.WriteLine();
                            int b = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine();

                            switch (b)
                            {

                                case 1:
                                    S.SurfaceArea();
                                    break;
                                case 2:
                                    S.Volume();
                                    break;
                            }
                            break;
                        }



                    case 2:
                        Shapes Cyl = new Cylinder();
                        {
                            Console.WriteLine();
                            Console.WriteLine("Choose one from choices : \n1 - Surface Ara \n2- Volume");
                            Console.WriteLine();
                            int b = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine();

                            switch (b)
                            {
                                case 1:
                                    Cyl.SurfaceArea();
                                    break;

                                case 2:
                                    Cyl.Volume();
                                    break;
                            }
                            break;
                        }

                    case 3:
                        Shapes Cub = new Cube();
                        {
                            Console.WriteLine();
                            Console.WriteLine("Choose one from choices : \n1 - Surface Ara \n2- Volume");
                            Console.WriteLine();
                            int b = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine();

                            switch (b)
                            {
                                case 1:
                                    Cub.SurfaceArea();
                                    break;
                                case 2:
                                    Cub.Volume();
                                    break;
                            }
                            break;
                        }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Choose one from choices : \n1 - Sphere \n2- Cylinder \n3- Cube");
                Console.WriteLine();
                int a = Convert.ToInt32(Console.ReadLine());
                switch (a)
                {
                    case 1:
                        Shapes S = new Sphere();
                        {
                            Console.WriteLine();
                            Console.WriteLine("Choose one from choices : \n1 - Surface Ara \n2- Volume");
                            Console.WriteLine();
                            int b = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine();

                            switch (b)
                            {

                                case 1:
                                    S.SurfaceArea();
                                    break;
                                case 2:
                                    S.Volume();
                                    break;
                            }
                            break;
                        }



                    case 2:
                        Shapes Cyl = new Cylinder();
                        {
                            Console.WriteLine();
                            Console.WriteLine("Choose one from choices : \n1 - Surface Ara \n2- Volume");
                            Console.WriteLine();
                            int b = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine();

                            switch (b)
                            {
                                case 1:
                                    Cyl.SurfaceArea();
                                    break;

                                case 2:
                                    Cyl.Volume();
                                    break;
                            }
                            break;
                        }

                    case 3:
                        Shapes Cub = new Cube();
                        {
                            Console.WriteLine();
                            Console.WriteLine("Choose one from choices : \n1 - Surface Ara \n2- Volume");
                            Console.WriteLine();
                            int b = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine();

                            switch (b)
                            {
                                case 1:
                                    Cub.SurfaceArea();
                                    break;
                                case 2:
                                    Cub.Volume();
                                    break;
                            }
                            break;

                        }

                }

            }
            Console.ReadLine();
            Area s = new rectangle();
            s.CalArea();
            Area q = new square();
            q.CalArea();
        }

    }





}






abstract public class Shapes
{
    public double radius, edge, height;
    public double pi = 3.14;
    public abstract void SurfaceArea();
    public abstract void Volume();
    

}

public class Sphere : Shapes
{
    public override void SurfaceArea()
    {


        Console.Write("Enter the Radius : ");
        radius = Convert.ToDouble(Console.ReadLine());
        Console.WriteLine();
        Console.WriteLine("Formula for calculating Surface Area of sphere is 4 * pi * radius * radius ");
        Console.WriteLine("Surface area of sphere is  : " + (4 * pi * radius * radius));
        Console.WriteLine();
    }
    public override void Volume()
    {
        Console.Write("Enter the Radius : ");
        radius = Convert.ToDouble(Console.ReadLine());
        Console.WriteLine();
        Console.WriteLine("Formula for calculating Volume of sphere is (4/3)Pi* Radius*Radius*Radius ");
        Console.WriteLine("Volume of sphere is  : " + (pi * radius * radius * radius * 4 / 3));
    }

}
public class Cylinder : Shapes
{
    public override void SurfaceArea()
    {
        Console.Write("Enter the Radius : ");
        radius = Convert.ToDouble(Console.ReadLine());
        Console.Write("Enter the height : ");
        height = Convert.ToDouble(Console.ReadLine());
        Console.WriteLine();
        Console.WriteLine("Formula for calculating Surface Area of Cylinder is 2πrh+2πr*r ");
        Console.WriteLine("Surface area of Cylinder is  : " + ((2 * pi * radius * height) + (2 * pi * radius * radius)));
    }
    public override void Volume()
    {
        Console.Write("Enter the Radius : ");
        radius = Convert.ToDouble(Console.ReadLine());
        Console.Write("Enter the height : ");
        height = Convert.ToDouble(Console.ReadLine());
        Console.WriteLine();
        Console.WriteLine("Formula for calculating Volume of Cylinder is  πr*rh ");
        Console.WriteLine("Volume of Cylinder is  : " + (pi * radius * radius * height));
    }

}
public class Cube : Shapes
{
    public override void SurfaceArea()
    {
        Console.Write("Enter the Edge : ");
        edge = Convert.ToDouble(Console.ReadLine());
        Console.WriteLine();
        Console.WriteLine("Formula for calculating Surface Area of Cube is 6 *edge*edge ");
        Console.WriteLine("Surface area of Cube is  : " + (6 * edge * edge));
    }
    public override void Volume()
    {
        Console.Write("Enter the Edge : ");
        edge = Convert.ToDouble(Console.ReadLine());
        Console.WriteLine();
        Console.WriteLine("Formula for calculating Volume of Cube is edge*edge*edge ");
        Console.WriteLine("Volume of Cube is  : " + (edge * edge * edge));
    }

}
//public class Square : Area
//{
//    public override void CalArea()
//    {
//        Console.Write("Enter the Side : ");
//        side = Convert.ToDouble(Console.ReadLine());
//        Console.WriteLine();
//        Console.WriteLine("Formula for calculating area of square is side*side ");
//        Console.WriteLine("Area of square is  : " + (side * side));

//    }
//}
//}public class Rectangle:Area      //interface se krna hai
//{
//    public override void CalArea()
//    {
//        Console.Write("Enter the length and breadth : ");
//        length = Convert.ToDouble(Console.ReadLine());               //Parse use krna hai
//        breadth = Convert.ToDouble(Console.ReadLine());
//        Console.WriteLine();
//        Console.WriteLine("Formula for calculating area of rectangle is length*breadth ");
//        Console.WriteLine("Area of square is  : " + (length * breadth ));

//    }
//}

public interface Area
{
    // public double side, length, breadth;
    void CalArea();
}


 public class rectangle : Area
{
    public void CalArea()
    {
        double length = Convert.ToDouble(Console.ReadLine());
        double breadth = Convert.ToDouble(Console.ReadLine());

        Console.WriteLine("Area of rectangle is "+ length * breadth);
            
    }
}public class square : Area
{
    public void CalArea()
    {
        double side = Convert.ToDouble(Console.ReadLine());
        

        Console.WriteLine("Area of Square is "+ side * side);
            
    }
}

