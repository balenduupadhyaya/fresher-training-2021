﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Party__management_system_ex_hn
{

    public class Party
    {
        public string bookingType;
        public int noOfPeople;
        public string partyVenue;
        public double decorationCharges;
        public double cateringCharges;

        public virtual void PartyPackage(string partyVenue)
        {

        }

        public virtual void DisplayPartyDetails(string packageName, double decorationCharges, double cateringCharges)
        {

        }

    }

    class Birthday : Party
    {
        int readVenue;
        int readPackage;
        string bookingDate;
        int bookingId;
        public void BirthdayOrganizing(int totalPeople, string date, int value)
        {
            try
            {
                bookingDate = date;
                bookingId = value;
                base.noOfPeople = totalPeople;
                Console.WriteLine("Welcome To Birthday Booking System");
                Console.WriteLine("--- Venue Available : ---");
                Console.WriteLine("1. The Oberoi, New Delhi");
                Console.WriteLine("2. ITC Maurya, A Luxury Collection Hotel, New Delhi");
                readVenue = Convert.ToInt32(Console.ReadLine());
                switch (readVenue)
                {
                    case 1:
                        base.partyVenue = "Reddisson Blue- Dwarka";
                        PartyPackage(base.partyVenue);
                        break;
                    case 2:
                        base.partyVenue = "HolidayInn Hotel- Gurgaon";
                        PartyPackage(base.partyVenue);
                        break;
                    default:
                        Console.WriteLine("Entered Wrong Choice ! Please Select Again :");
                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                bookingDate = date;
                bookingId = value;
                base.noOfPeople = totalPeople;
                Console.WriteLine("Welcome To Birthday Booking System");
                Console.WriteLine("--- Venue Available : ---");
                Console.WriteLine("1. The Oberoi, New Delhi");
                Console.WriteLine("2. ITC Maurya, A Luxury Collection Hotel, New Delhi");
                readVenue = Convert.ToInt32(Console.ReadLine());
                switch (readVenue)
                {
                    case 1:
                        base.partyVenue = "Reddisson Blue- Dwarka";
                        PartyPackage(base.partyVenue);
                        break;
                    case 2:
                        base.partyVenue = "HolidayInn Hotel- Gurgaon";
                        PartyPackage(base.partyVenue);
                        break;
                    default:
                        Console.WriteLine("Entered Wrong Choice ! Please Select Again :");
                        break;
                }
            }

        }

        public override void PartyPackage(string partyVenue)
        {
            try
            {
                string checkCondition = "n";
                do
                {
                    Console.WriteLine("Select Packages : ");
                    Console.WriteLine("1. Silver Package ");
                    Console.WriteLine("2. Gold Package : ");
                    Console.WriteLine("3. Platinum Package : ");
                    readPackage = Convert.ToInt32(Console.ReadLine());

                    switch (readPackage)
                    {

                        case 1:
                            SilverPackage();
                            break;
                        case 2:
                            GoldPackage();
                            break;
                        case 3:
                            PlatinumPackage();
                            break;
                        default:
                            Console.WriteLine("Entered Wrong Choice ! Enter [y/Y] to Select choice again :");
                            checkCondition = Console.ReadLine();
                            break;
                    }
                } while (checkCondition == "y" || checkCondition == "Y");
            }
            catch (Exception e)
            {
                string checkCondition = "n";
                do
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Select Packages : ");
                    Console.WriteLine("1. Silver Package ");
                    Console.WriteLine("2. Gold Package : ");
                    Console.WriteLine("3. Platinum Package : ");
                    readPackage = Convert.ToInt32(Console.ReadLine());

                    switch (readPackage)
                    {

                        case 1:
                            SilverPackage();
                            break;
                        case 2:
                            GoldPackage();
                            break;
                        case 3:
                            PlatinumPackage();
                            break;
                        default:
                            Console.WriteLine("Entered Wrong Choice ! Enter [y/Y] to Select choice again :");
                            checkCondition = Console.ReadLine();
                            break;
                    }
                } while (checkCondition == "y" || checkCondition == "Y");
            }
            void SilverPackage()
            {
                string packageSilver = "Silver Package";
                base.decorationCharges = 10000;
                base.cateringCharges = 20000;
                DisplayPartyDetails(packageSilver, base.decorationCharges, base.cateringCharges);

            }
            void GoldPackage()
            {
                string packageGold = "Gold Package";
                base.decorationCharges = 12000;
                base.cateringCharges = 25000;
                DisplayPartyDetails(packageGold, base.decorationCharges, base.cateringCharges);
            }
            void PlatinumPackage()
            {
                string packagePlatinum = "Platinum Package";
                base.decorationCharges = 15000;
                base.cateringCharges = 30000;
                DisplayPartyDetails(packagePlatinum, base.decorationCharges, base.cateringCharges);
            }



        }
        public override void DisplayPartyDetails(string packageName, double decorationCharges, double cateringCharges)
        {
            if (packageName == "Silver Package")
            {
                Console.WriteLine("--- Booking is Confirmed ---");
                Console.WriteLine("Party Type : Birthday Party");
                Console.WriteLine("Booking ID : " + bookingId);
                Console.WriteLine("Package : " + packageName);
                Console.WriteLine("Venue : " + base.partyVenue);
                Console.WriteLine("Booking Date : " + bookingDate);
                double totalCost = decorationCharges + cateringCharges;
                Console.WriteLine("Cost Per Person :" + totalCost / base.noOfPeople);
                Console.WriteLine("Includes Unlimited Buffe and Extra One Diffrent Snacks");
                Console.WriteLine();
            }
            else if (packageName == "Gold Package")
            {
                Console.WriteLine("--- Booking is Confirmed ---");
                Console.WriteLine("Party Type : Birthday Party");
                Console.WriteLine("Booking ID : " + bookingId);
                Console.WriteLine("Package : " + packageName);
                Console.WriteLine("Venue : " + base.partyVenue);
                Console.WriteLine("Booking Date : " + bookingDate);
                double totalCost = decorationCharges + cateringCharges;
                Console.WriteLine("Cost Per Person :" + totalCost / base.noOfPeople);
                Console.WriteLine("Includes Unlimited Buffe   Mocktails and Extra One Diffrent Snacks");
                Console.WriteLine();
            }
            else
            {
                Console.WriteLine("--- Booking is Confirmed ---");
                Console.WriteLine("Party Type : Birthday Party");
                Console.WriteLine("Booking ID : " + bookingId);
                Console.WriteLine("Package : " + packageName);
                Console.WriteLine("Venue : " + base.partyVenue);
                Console.WriteLine("Booking Date : " + bookingDate);
                double totalCost = decorationCharges + cateringCharges;
                Console.WriteLine("Cost Per Person :" + totalCost / base.noOfPeople);
                Console.WriteLine("Includes Unlimited Buffe   Mocktails   Cocktails and Extra Two Diffrent Snacks");
                Console.WriteLine("Free Goodies");
            }
        }
    }
    class Anniversary : Party
    {
        int readVenue;
        int readPackage;
        string bookingDate;
        int bookingId;
        public void AnniversaryOrganizing(int totalPeople, string date, int value)
        {
            try
            {
                bookingDate = date;
                bookingId = value;
                base.noOfPeople = totalPeople;
                Console.WriteLine("wElCoMe To AnNiVeRsArY BoOkInG SyStEm");
                Console.WriteLine("--- Venue Available : ---");
                Console.WriteLine("1. The Oberoi, New Delhi");
                Console.WriteLine("2. ITC Maurya, A Luxury Collection Hotel, New Delhi");
                readVenue = Convert.ToInt32(Console.ReadLine());
                switch (readVenue)
                {
                    case 1:
                        base.partyVenue = "Reddisson Blue- Dwarka";
                        PartyPackage(base.partyVenue);
                        break;
                    case 2:
                        base.partyVenue = "HolidayInn Hotel- Gurgaon";
                        PartyPackage(base.partyVenue);
                        break;
                    default:
                        Console.WriteLine("Entered Wrong Choice ! Please Select Again :");
                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                bookingDate = date;
                bookingId = value;
                base.noOfPeople = totalPeople;
                Console.WriteLine("wElCoMe To AnNiVeRsArY BoOkInG SyStEm");
                Console.WriteLine("--- Venue Available : ---");
                Console.WriteLine("1. The Oberoi, New Delhi");
                Console.WriteLine("2. ITC Maurya, A Luxury Collection Hotel, New Delhi");
                readVenue = Convert.ToInt32(Console.ReadLine());
                switch (readVenue)
                {
                    case 1:
                        base.partyVenue = "Reddisson Blue- Dwarka";
                        PartyPackage(base.partyVenue);
                        break;
                    case 2:
                        base.partyVenue = "HolidayInn Hotel- Gurgaon";
                        PartyPackage(base.partyVenue);
                        break;
                    default:
                        Console.WriteLine("Entered Wrong Choice ! Please Select Again :");
                        break;
                }
            }

        }

        public override void PartyPackage(string partyVenue)
        {
            try
            {
                Console.WriteLine("Select Packages : ");
                Console.WriteLine("1. Silver Package ");
                Console.WriteLine("2. Gold Package : ");
                Console.WriteLine("3. Platinum Package : ");
                readPackage = Convert.ToInt32(Console.ReadLine());

                switch (readPackage)
                {
                    case 1:
                        SilverPackage();
                        break;
                    case 2:
                        GoldPackage();
                        break;
                    case 3:
                        PlatinumPackage();
                        break;
                    default:
                        Console.WriteLine("Entered Wrong Choice ! Please Select Again :");
                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Select Packages : ");
                Console.WriteLine("1. Silver Package ");
                Console.WriteLine("2. Gold Package : ");
                Console.WriteLine("3. Platinum Package : ");
                readPackage = Convert.ToInt32(Console.ReadLine());

                switch (readPackage)
                {
                    case 1:
                        SilverPackage();
                        break;
                    case 2:
                        GoldPackage();
                        break;
                    case 3:
                        PlatinumPackage();
                        break;
                    default:
                        Console.WriteLine("Entered Wrong Choice ! Please Select Again :");
                        break;
                }
            }

            void SilverPackage()
            {
                string packageSilver = "Silver Package";
                base.decorationCharges = 15000;
                base.cateringCharges = 50000;
                DisplayPartyDetails(packageSilver, base.decorationCharges, base.cateringCharges);

            }
            void GoldPackage()
            {
                string packageGold = "Gold Package";
                base.decorationCharges = 20000;
                base.cateringCharges = 75000;
                DisplayPartyDetails(packageGold, base.decorationCharges, base.cateringCharges);
            }
            void PlatinumPackage()
            {
                string packagePlatinum = "Platinum Package";
                base.decorationCharges = 30000;
                base.cateringCharges = 100000;
                DisplayPartyDetails(packagePlatinum, base.decorationCharges, base.cateringCharges);
            }

        }
        public override void DisplayPartyDetails(string packageName, double decorationCharges, double cateringCharges)
        {
            if (packageName == "Silver Package" && noOfPeople <= 150)
            {
                Console.WriteLine("--- Booking is Confirmed ---");
                Console.WriteLine("Party Type : Anniversary Party");
                Console.WriteLine("Booking ID : " + bookingId);
                Console.WriteLine("Package : " + packageName);
                Console.WriteLine("Venue : " + base.partyVenue);
                Console.WriteLine("Booking Date : " + bookingDate);
                double totalCost = decorationCharges + cateringCharges;
                Console.WriteLine("Cost Per Person :" + totalCost / base.noOfPeople);
                Console.WriteLine("            INCLUDES       ");
                Console.WriteLine("Includes Unlimited Buffe and Extra One Diffrent Snacks");
                Console.WriteLine("Maximum 150 Peoples Allowed");
                Console.WriteLine();
            }
            else if (packageName == "Gold Package" && noOfPeople > 150 && noOfPeople <= 250)
            {
                Console.WriteLine("--- Booking is Confirmed ---");
                Console.WriteLine("Party Type : Anniversary Party");
                Console.WriteLine("Booking ID : " + bookingId);
                Console.WriteLine("Package : " + packageName);
                Console.WriteLine("Venue : " + base.partyVenue);
                Console.WriteLine("Booking Date : " + bookingDate);
                double totalCost = decorationCharges + cateringCharges;
                Console.WriteLine("Cost Per Person :" + totalCost / base.noOfPeople);
                Console.WriteLine("            INCLUDES       ");
                Console.WriteLine("Includes Unlimited Buffe   Mocktails and Extra One Diffrent Snacks");
                Console.WriteLine("Gather upto 250 peoples");
                Console.WriteLine();
            }
            else
            {
                Console.WriteLine("--- Booking is Confirmed ---");
                Console.WriteLine("Party Type : Anniversary Party");
                Console.WriteLine("Booking ID : " + bookingId);
                Console.WriteLine("Package : " + packageName);
                Console.WriteLine("Venue : " + base.partyVenue);
                Console.WriteLine("Booking Date : " + bookingDate);
                double totalCost = decorationCharges + cateringCharges;
                Console.WriteLine("Cost Per Person :" + totalCost / base.noOfPeople);
                Console.WriteLine("            INCLUDES       ");
                Console.WriteLine("Includes Unlimited Buffe   Mocktails   Cocktails and Extra Two Diffrent Snacks");
                Console.WriteLine("Free Parking Facility");
                Console.WriteLine("Gather upto 500 peoples");
                Console.WriteLine();
            }
        }
    }
    class Retirement : Party
    {
        int readVenue;
        int readPackage;
        string bookingDate;
        int bookingId;
        public void RetirementOrganizing(int totalPeople, string date, int value)
        {
            try
            {
                bookingDate = date;
                bookingId = value;
                base.noOfPeople = totalPeople;
                Console.WriteLine("Welcome To Retirement Party Booking System");
                Console.WriteLine("--- Venue Available : ---");
                Console.WriteLine("1. The Oberoi, New Delhi");
                Console.WriteLine("2. ITC Maurya, A Luxury Collection Hotel, New Delhi");
                readVenue = Convert.ToInt32(Console.ReadLine());
                switch (readVenue)
                {
                    case 1:
                        base.partyVenue = "Reddisson Blue- Dwarka";
                        PartyPackage(base.partyVenue);
                        break;
                    case 2:
                        base.partyVenue = "HolidayInn Hotel- Gurgaon";
                        PartyPackage(base.partyVenue);
                        break;
                    default:
                        Console.WriteLine("Entered Wrong Choice ! Please Select Again :");
                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                bookingDate = date;
                bookingId = value;
                base.noOfPeople = totalPeople;
                Console.WriteLine("Welcome To Retirement Party Booking System");
                Console.WriteLine("--- Venue Available : ---");
                Console.WriteLine("1. The Oberoi, New Delhi");
                Console.WriteLine("2. ITC Maurya, A Luxury Collection Hotel, New Delhi");
                readVenue = Convert.ToInt32(Console.ReadLine());
                switch (readVenue)
                {
                    case 1:
                        base.partyVenue = "Reddisson Blue- Dwarka";
                        PartyPackage(base.partyVenue);
                        break;
                    case 2:
                        base.partyVenue = "HolidayInn Hotel- Gurgaon";
                        PartyPackage(base.partyVenue);
                        break;
                    default:
                        Console.WriteLine("Entered Wrong Choice ! Please Select Again :");
                        break;
                }

            }
        }

        public override void PartyPackage(string partyVenue)
        {
            try
            {
                Console.WriteLine("Select Packages : ");
                Console.WriteLine("1. Silver Package ");
                Console.WriteLine("2. Gold Package : ");
                Console.WriteLine("3. Platinum Package : ");
                readPackage = Convert.ToInt32(Console.ReadLine());

                switch (readPackage)
                {
                    case 1:
                        SilverPackage();
                        break;
                    case 2:
                        GoldPackage();
                        break;
                    case 3:
                        PlatinumPackage();
                        break;
                    default:
                        Console.WriteLine("Entered Wrong Choice ! Please Select Again :");
                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Select Packages : ");
                Console.WriteLine("1. Silver Package ");
                Console.WriteLine("2. Gold Package : ");
                Console.WriteLine("3. Platinum Package : ");
                readPackage = Convert.ToInt32(Console.ReadLine());

                switch (readPackage)
                {
                    case 1:
                        SilverPackage();
                        break;
                    case 2:
                        GoldPackage();
                        break;
                    case 3:
                        PlatinumPackage();
                        break;
                    default:
                        Console.WriteLine("Entered Wrong Choice ! Please Select Again :");
                        break;
                }
            }

            void SilverPackage()
            {
                string packageSilver = "Silver Package";
                base.decorationCharges = 17000;
                base.cateringCharges = 55000;
                DisplayPartyDetails(packageSilver, base.decorationCharges, base.cateringCharges);

            }
            void GoldPackage()
            {
                string packageGold = "Gold Package";
                base.decorationCharges = 22000;
                base.cateringCharges = 85000;
                DisplayPartyDetails(packageGold, base.decorationCharges, base.cateringCharges);
            }
            void PlatinumPackage()
            {
                string packagePlatinum = "Platinum Package";
                base.decorationCharges = 35000;
                base.cateringCharges = 200000;
                DisplayPartyDetails(packagePlatinum, base.decorationCharges, base.cateringCharges);
            }

        }
        public override void DisplayPartyDetails(string packageName, double decorationCharges, double cateringCharges)
        {
            if (packageName == "Silver Package" && noOfPeople <= 150)
            {
                Console.WriteLine("--- Booking is Confirmed ---");
                Console.WriteLine("Party Type : Retirement Party");
                Console.WriteLine("Booking ID : " + bookingId);
                Console.WriteLine("Package : " + packageName);
                Console.WriteLine("Venue : " + base.partyVenue);
                Console.WriteLine("Booking Date : " + bookingDate);
                double totalCost = decorationCharges + cateringCharges;
                Console.WriteLine("Cost Per Person :" + totalCost / base.noOfPeople);
                Console.WriteLine("            INCLUDES       ");
                Console.WriteLine("Includes Unlimited Buffe  and Extra one Diffrent Snacks");
                Console.WriteLine("Maximum 200 Peoples Allowed");
                Console.WriteLine();
            }
            else if (packageName == "Gold Package" && noOfPeople > 150 && noOfPeople <= 250)
            {
                Console.WriteLine("--- Booking is Confirmed ---");
                Console.WriteLine("Party Type : Retirement Party");
                Console.WriteLine("Booking ID : " + bookingId);
                Console.WriteLine("Package : " + packageName);
                Console.WriteLine("Venue : " + base.partyVenue);
                Console.WriteLine("Booking Date : " + bookingDate);
                double totalCost = decorationCharges + cateringCharges;
                Console.WriteLine("Cost Per Person :" + totalCost / base.noOfPeople);
                Console.WriteLine("            INCLUDES       ");
                Console.WriteLine("Includes Unlimited Buffe   Mocktails and Extra One Diffrent Snacks");
                Console.WriteLine("Gather upto 250 peoples");
                Console.WriteLine();
            }
            else
            {
                Console.WriteLine("--- Booking is Confirmed ---");
                Console.WriteLine("Party Type : Retirement Party");
                Console.WriteLine("Booking ID : " + bookingId);
                Console.WriteLine("Package : " + packageName);
                Console.WriteLine("Venue : " + base.partyVenue);
                Console.WriteLine("Booking Date : " + bookingDate);
                double totalCost = decorationCharges + cateringCharges;
                Console.WriteLine("Cost Per Person :" + totalCost / base.noOfPeople);
                Console.WriteLine("            INCLUDES       ");
                Console.WriteLine("Includes Unlimited Buffe   Mocktails   Cocktails  and Extra Two Diffrent Snacks");
                Console.WriteLine("Free Parking Facility");
                Console.WriteLine("Gather upto 500 peoples");
                Console.WriteLine();
            }
        }
    }
    class Farewell : Party
    {
        int readVenue;
        int readPackage;
        string bookingDate;
        int bookingId;
        public void FarewellOrganizing(int totalPeople, string date, int value)
        {
            try
            {
                bookingDate = date;
                bookingId = value;
                base.noOfPeople = totalPeople;
                Console.WriteLine("Welcome To Farewell Party Booking System");
                Console.WriteLine("--- Venue Available : ---");
                Console.WriteLine("1. The Oberoi, New Delhi");
                Console.WriteLine("2. ITC Maurya, A Luxury Collection Hotel, New Delhi");
                readVenue = Convert.ToInt32(Console.ReadLine());
                switch (readVenue)
                {
                    case 1:
                        base.partyVenue = "Reddisson Blue- Dwarka";
                        PartyPackage(base.partyVenue);
                        break;
                    case 2:
                        base.partyVenue = "HolidayInn Hotel- Gurgaon";
                        PartyPackage(base.partyVenue);
                        break;
                    default:
                        Console.WriteLine("Entered Wrong Choice ! Please Select Again :");
                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                bookingDate = date;
                bookingId = value;
                base.noOfPeople = totalPeople;
                Console.WriteLine("Welcome To Farewell Party Booking System");
                Console.WriteLine("--- Venue Available : ---");
                Console.WriteLine("1. The Oberoi, New Delhi");
                Console.WriteLine("2. ITC Maurya, A Luxury Collection Hotel, New Delhi");
                readVenue = Convert.ToInt32(Console.ReadLine());
                switch (readVenue)
                {
                    case 1:
                        base.partyVenue = "Reddisson Blue- Dwarka";
                        PartyPackage(base.partyVenue);
                        break;
                    case 2:
                        base.partyVenue = "HolidayInn Hotel- Gurgaon";
                        PartyPackage(base.partyVenue);
                        break;
                    default:
                        Console.WriteLine("Entered Wrong Choice ! Please Select Again :");
                        break;
                }
            }

        }

        public override void PartyPackage(string partyVenue)
        {
            try
            {
                Console.WriteLine("Select Packages : ");
                Console.WriteLine("1. Silver Package ");
                Console.WriteLine("2. Gold Package : ");
                Console.WriteLine("3. Platinum Package : ");
                readPackage = Convert.ToInt32(Console.ReadLine());

                switch (readPackage)
                {
                    case 1:
                        SilverPackage();
                        break;
                    case 2:
                        GoldPackage();
                        break;
                    case 3:
                        PlatinumPackage();
                        break;
                    default:
                        Console.WriteLine("Entered Wrong Choice ! Please Select Again :");
                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Select Packages : ");
                Console.WriteLine("1. Silver Package ");
                Console.WriteLine("2. Gold Package : ");
                Console.WriteLine("3. Platinum Package : ");
                readPackage = Convert.ToInt32(Console.ReadLine());

                switch (readPackage)
                {
                    case 1:
                        SilverPackage();
                        break;
                    case 2:
                        GoldPackage();
                        break;
                    case 3:
                        PlatinumPackage();
                        break;
                    default:
                        Console.WriteLine("Entered Wrong Choice ! Please Select Again :");
                        break;
                }
            }

            void SilverPackage()
            {
                string packageSilver = "Silver Package";
                base.decorationCharges = 10000;
                base.cateringCharges = 35000;
                DisplayPartyDetails(packageSilver, base.decorationCharges, base.cateringCharges);

            }
            void GoldPackage()
            {
                string packageGold = "Gold Package";
                base.decorationCharges = 15000;
                base.cateringCharges = 50000;
                DisplayPartyDetails(packageGold, base.decorationCharges, base.cateringCharges);
            }
            void PlatinumPackage()
            {
                string packagePlatinum = "Platinum Package";
                base.decorationCharges = 30000;
                base.cateringCharges = 200000;
                DisplayPartyDetails(packagePlatinum, base.decorationCharges, base.cateringCharges);
            }

        }
        public override void DisplayPartyDetails(string packageName, double decorationCharges, double cateringCharges)
        {
            if (packageName == "Silver Package" && noOfPeople <= 150)
            {
                Console.WriteLine("--- Booking is Confirmed ---");
                Console.WriteLine("Party Type : Farewell Party");
                Console.WriteLine("Booking ID : " + bookingId);
                Console.WriteLine("Package : " + packageName);
                Console.WriteLine("Venue : " + base.partyVenue);
                Console.WriteLine("Booking Date : " + bookingDate);
                double totalCost = decorationCharges + cateringCharges;
                Console.WriteLine("Cost Per Person :" + totalCost / base.noOfPeople);
                Console.WriteLine("            INCLUDES       ");
                Console.WriteLine("Includes Unlimited Buffe  and Extra one Diffrent Snacks");
                Console.WriteLine("Maximum 200 Peoples Allowed");
                Console.WriteLine();
            }
            else if (packageName == "Gold Package" && noOfPeople > 150 && noOfPeople <= 250)
            {
                Console.WriteLine("--- Booking is Confirmed ---");
                Console.WriteLine("Party Type : Farewell Party");
                Console.WriteLine("Booking ID : " + bookingId);
                Console.WriteLine("Package : " + packageName);
                Console.WriteLine("Venue : " + base.partyVenue);
                Console.WriteLine("Booking Date : " + bookingDate);
                double totalCost = decorationCharges + cateringCharges;
                Console.WriteLine("Cost Per Person :" + totalCost / base.noOfPeople);
                Console.WriteLine("            INCLUDES       ");
                Console.WriteLine("Includes Unlimited Buffe   Mocktails and Extra One Diffrent Snacks");
                Console.WriteLine("Gather upto 250 peoples");
                Console.WriteLine();
            }
            else
            {
                Console.WriteLine("--- Booking is Confirmed ---");
                Console.WriteLine("Party Type : Farewell Party");
                Console.WriteLine("Booking ID : " + bookingId);
                Console.WriteLine("Package : " + packageName);
                Console.WriteLine("Venue : " + base.partyVenue);
                Console.WriteLine("Booking Date : " + bookingDate);
                double totalCost = decorationCharges + cateringCharges;
                Console.WriteLine("Cost Per Person :" + totalCost / base.noOfPeople);
                Console.WriteLine("            INCLUDES       ");
                Console.WriteLine("Includes Unlimited Buffe   Mocktails   Cocktails  and Extra Two Diffrent Snacks");
                Console.WriteLine("Free Parking Facility");
                Console.WriteLine("Gather upto 500 peoples");
                Console.WriteLine();
            }
        }
    }
    public class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string condition;
                DateTime now = DateTime.Now;
                Random rnd = new Random();
                int value;
                string currentDate = now.ToString("F");
                do
                {
                    Console.WriteLine("*******WELCOME TO SUCCESSIVE PARTY MANAGEMENT SYSTEM*******");
                    Console.WriteLine();
                    Console.WriteLine("Select your Booking Category");
                    Console.WriteLine("1. Birthday Party");
                    Console.WriteLine("2. Anniversary Party");
                    Console.WriteLine("3. Retirement Party");
                    Console.WriteLine("4. Farewell Party");
                    int bookingType = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine();
                    Birthday bObje = new Birthday();
                    Anniversary anObj = new Anniversary();
                    Retirement obj3 = new Retirement();
                    Farewell obj4 = new Farewell();
                    int noOfPeople;
                    switch (bookingType)
                    {
                        case 1:

                            try
                            {
                                Console.WriteLine("Enter Number of Peoples");
                                noOfPeople = Convert.ToInt32(Console.ReadLine());
                                value = rnd.Next(100000, 999999);
                                bObje.BirthdayOrganizing(noOfPeople, currentDate, value);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                                noOfPeople = Convert.ToInt32(Console.ReadLine());
                                value = rnd.Next(100000, 999999);
                                bObje.BirthdayOrganizing(noOfPeople, currentDate, value);
                            }

                            break;
                        case 2:

                            try
                            {
                                Console.WriteLine("Enter Number of Peoples");
                                noOfPeople = Convert.ToInt32(Console.ReadLine());
                                value = rnd.Next(100000, 999999);
                                anObj.AnniversaryOrganizing(noOfPeople, currentDate, value);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                                Console.WriteLine("Enter Number of Peoples");
                                noOfPeople = Convert.ToInt32(Console.ReadLine());
                                value = rnd.Next(100000, 999999);
                                anObj.AnniversaryOrganizing(noOfPeople, currentDate, value);
                            }
                            break;
                        case 3:
                            try
                            {
                                Console.WriteLine("Enter Number of Peoples");
                                noOfPeople = Convert.ToInt32(Console.ReadLine());
                                value = rnd.Next(100000, 999999);
                                obj3.RetirementOrganizing(noOfPeople, currentDate, value);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                                Console.WriteLine("Enter Number of Peoples");
                                noOfPeople = Convert.ToInt32(Console.ReadLine());
                                value = rnd.Next(100000, 999999);
                                obj3.RetirementOrganizing(noOfPeople, currentDate, value);
                            }

                            break;
                        case 4:
                            try
                            {
                                Console.WriteLine("Enter Number of Peoples");
                                noOfPeople = Convert.ToInt32(Console.ReadLine());
                                value = rnd.Next(100000, 999999);
                                obj4.FarewellOrganizing(noOfPeople, currentDate, value);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                                Console.WriteLine("Enter Number of Peoples");
                                noOfPeople = Convert.ToInt32(Console.ReadLine());
                                value = rnd.Next(100000, 999999);
                                obj4.FarewellOrganizing(noOfPeople, currentDate, value);
                            }

                            break;
                        default:
                            Console.WriteLine("Wrong Choice Selected ! Please Select Again :");
                            break;
                    }
                    Console.WriteLine("Do You Want to Book Party Again...");
                    condition = Console.ReadLine();
                }
                while (condition == "yes" || condition == "Yes");
                Console.ReadLine();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                string condition;
                DateTime now = DateTime.Now;
                Random rnd = new Random();
                int value;
                string currentDate = now.ToString("F");
                do
                {
                    Console.WriteLine("*******WELCOME TO SUCCESSIVE PARTY MANAGEMENT SYSTEM*******");
                    Console.WriteLine();
                    Console.WriteLine("Select your Booking Category");
                    Console.WriteLine("1. Birthday Party");
                    Console.WriteLine("2. Anniversary Party");
                    Console.WriteLine("3. Retirement Party");
                    Console.WriteLine("4. Farewell Party");
                    int bookingType = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine();
                    Birthday bObje = new Birthday();
                    Anniversary anObj = new Anniversary();
                    Retirement obj3 = new Retirement();
                    Farewell obj4 = new Farewell();
                    int noOfPeople;
                    switch (bookingType)
                    {
                        case 1:

                            try
                            {
                                Console.WriteLine("Enter Number of Peoples");
                                noOfPeople = Convert.ToInt32(Console.ReadLine());
                                value = rnd.Next(100000, 999999);
                                bObje.BirthdayOrganizing(noOfPeople, currentDate, value);
                            }
                            catch (Exception g)
                            {
                                Console.WriteLine(g.Message);
                                noOfPeople = Convert.ToInt32(Console.ReadLine());
                                value = rnd.Next(100000, 999999);
                                bObje.BirthdayOrganizing(noOfPeople, currentDate, value);
                            }

                            break;
                        case 2:

                            try
                            {
                                Console.WriteLine("Enter Number of Peoples");
                                noOfPeople = Convert.ToInt32(Console.ReadLine());
                                value = rnd.Next(100000, 999999);
                                anObj.AnniversaryOrganizing(noOfPeople, currentDate, value);
                            }
                            catch (Exception g)
                            {
                                Console.WriteLine(g.Message);
                                Console.WriteLine("Enter Number of Peoples");
                                noOfPeople = Convert.ToInt32(Console.ReadLine());
                                value = rnd.Next(100000, 999999);
                                anObj.AnniversaryOrganizing(noOfPeople, currentDate, value);
                            }
                            break;
                        case 3:
                            try
                            {
                                Console.WriteLine("Enter Number of Peoples");
                                noOfPeople = Convert.ToInt32(Console.ReadLine());
                                value = rnd.Next(100000, 999999);
                                obj3.RetirementOrganizing(noOfPeople, currentDate, value);
                            }
                            catch (Exception g)
                            {
                                Console.WriteLine(g.Message);
                                Console.WriteLine("Enter Number of Peoples");
                                noOfPeople = Convert.ToInt32(Console.ReadLine());
                                value = rnd.Next(100000, 999999);
                                obj3.RetirementOrganizing(noOfPeople, currentDate, value);
                            }

                            break;
                        case 4:
                            try
                            {
                                Console.WriteLine("Enter Number of Peoples");
                                noOfPeople = Convert.ToInt32(Console.ReadLine());
                                value = rnd.Next(100000, 999999);
                                obj4.FarewellOrganizing(noOfPeople, currentDate, value);
                            }
                            catch (Exception g)
                            {
                                Console.WriteLine(g.Message);
                                Console.WriteLine("Enter Number of Peoples");
                                noOfPeople = Convert.ToInt32(Console.ReadLine());
                                value = rnd.Next(100000, 999999);
                                obj4.FarewellOrganizing(noOfPeople, currentDate, value);
                            }

                            break;
                        default:
                            Console.WriteLine("Wrong Choice Selected ! Please Select Again :");
                            break;

                    }
                    Console.WriteLine("Do You Want to Book Party Again...");
                    condition = Console.ReadLine();
                }
                while (condition == "yes" || condition == "Yes");
                Console.ReadLine();

            }


        }
    }
}