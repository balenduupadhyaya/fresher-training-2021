﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PartyManagament
{
    class Party
    {
        public int nOfPeople;
        public string venue;
        public double decorationCharge;
        public double cateringCharge;
        public virtual void PartyPackage() { }
        public virtual void DisplayDetails(string packageName, double decorationCharge, double cateringCharge) { }

    }
    class Birthday : Party
    {
        int rvenue;
        int rpackage;
        int bookingId;
        string bookingDate;

        public void BirthdayArrangment(string date, int totalPeople, int value)
        {
            bookingDate = date;
            bookingId = value;
            base.nOfPeople = totalPeople;
            Console.WriteLine("Choose your venue");
            Console.WriteLine("1. Fortune INN ,ghaziabad");
            Console.WriteLine("2. Lella Palace,gurgaon");
            Console.WriteLine("3. Ajanta,delhi");
            rvenue = Convert.ToInt32(Console.ReadLine());
            switch (rvenue)
            {
                case 1:
                    base.venue = "Roseate house,gurgaon";
                    PartyPackage();
                    break;
                case 2:
                    base.venue = "Holiday-inn,gurgaon";
                    PartyPackage();
                    break;
                case 3:
                    base.venue = "Neemrana,karnal";
                    PartyPackage();
                    break;
                default:
                    Console.WriteLine("Entered wrong choice");
                    break;
            }
        }
        public override void PartyPackage()
        {
            string condition = "n";
            do
            {
                Console.WriteLine("Which package you would like to select??");
                Console.WriteLine("1.Silver package");
                Console.WriteLine("2.Gold package");
                Console.WriteLine("3.Platinum package");
                rpackage = Convert.ToInt32(Console.ReadLine());
                switch (rpackage)
                {
                    case 1:
                        SilverPackage();
                        break;
                    case 2:
                        GoldPackage();
                        break;
                    case 3:
                        PlatinumPackage();
                        break;
                    default:
                        Console.WriteLine("Entered wrong choice,try again");
                        condition = Console.ReadLine();
                        break;
                }

            } while (condition == "y" || condition == "Y");
            void SilverPackage()
            {
                string packageSil = "Silver package";
                base.decorationCharge = 17000;
                base.cateringCharge = 190000;
                DisplayDetails(packageSil, base.decorationCharge, base.cateringCharge);
            }
            void GoldPackage()
            {
                string packageGol = "Gold package";
                base.decorationCharge = 19000;
                base.cateringCharge = 210000;
                DisplayDetails(packageGol, base.decorationCharge, base.cateringCharge);
            }
            void PlatinumPackage()
            {
                string packagePla = "Platinum package";
                base.decorationCharge = 20000;
                base.cateringCharge = 250000;
                DisplayDetails(packagePla, base.decorationCharge, base.cateringCharge);
            }
        }
        public override void DisplayDetails(string packageName, double decorationCharge, double cateringCharge)
        {
            if (packageName == "Silver package")
            {
                Console.WriteLine("Congrats, your booking is done!!!!");
                Console.WriteLine("Booking type : Birthday party");
                Console.WriteLine("Booking date :" + bookingDate);
                Console.WriteLine("Package type :" + packageName);
                Console.WriteLine("Party venue :" + base.venue);
                double totalCost = decorationCharge + cateringCharge;
                Console.WriteLine("Your total bill is :" + totalCost);
                Console.WriteLine("Thank you for booking with us, We hope yo enjoy our service");

            }
            else if (packageName == "Gold package")
            {
                Console.WriteLine("Congrats, your booking is done!!!!");
                Console.WriteLine("Booking type : Birthday party");
                Console.WriteLine("Booking date :" + bookingDate);
                Console.WriteLine("Package type :" + packageName);
                Console.WriteLine("Party venue :" + base.venue);
                double totalCost = decorationCharge + cateringCharge;
                Console.WriteLine("Your total bill is :" + totalCost);
                Console.WriteLine("Thank you for booking with us, We hope yo enjoy our service");
            }
            else
            {
                Console.WriteLine("Congrats, your booking is done!!!!");
                Console.WriteLine("Booking type : Birthday party");
                Console.WriteLine("Booking date :" + bookingDate);
                Console.WriteLine("Package type :" + packageName);
                Console.WriteLine("Party venue :" + base.venue);
                double totalCost = decorationCharge + cateringCharge;
                Console.WriteLine("Your total bill is :" + totalCost);
                Console.WriteLine("Thank you for booking with us, We hope yo enjoy our service");
            }

        }
    }
    class Anniversary : Party
    {
        int rvenue;
        int rpackage;
        int bookingId;
        string bookingDate;
        public void AnniversaryArrangment(string date, int totalPeople, int value)
        {
            bookingDate = date;
            bookingId = value;
            base.nOfPeople = totalPeople;

            Console.WriteLine("Choose your venue");
            Console.WriteLine("1. Fortune INN ,ghaziabad");
            Console.WriteLine("2 Lella Palace,gurgaon");
            Console.WriteLine("3. Ajanta,delhi");
            rvenue = Convert.ToInt32(Console.ReadLine());
            switch (rvenue)
            {
                case 1:
                    base.venue = "Fortune INN ,ghaziabad";
                    PartyPackage();
                    break;
                case 2:
                    base.venue = "Lella Palace,gurgaon";
                    PartyPackage();
                    break;
                case 3:
                    base.venue = "Ajanta,delhi";
                    PartyPackage();
                    break;
                default:
                    Console.WriteLine("Entered wrong choice");
                    break;
            }
        }
        public override void PartyPackage()
        {
            string condition = "n";
            do
            {
                Console.WriteLine("Which package you would like to select??");
                Console.WriteLine("1.Silver package");
                Console.WriteLine("2.Gold package");
                Console.WriteLine("3.Platinum package");
                rpackage = Convert.ToInt32(Console.ReadLine());
                switch (rpackage)
                {
                    case 1:
                        SilverPackage();
                        break;
                    case 2:
                        GoldPackage();
                        break;
                    case 3:
                        PlatinumPackage();
                        break;
                    default:
                        Console.WriteLine("Entered wrong choice,try again");
                        condition = Console.ReadLine();
                        break;
                }

            } while (condition == "y" || condition == "Y");
            void SilverPackage()
            {
                string packageSil = "Silver package";
                base.decorationCharge = 11000;
                base.cateringCharge = 160000;
                DisplayDetails(packageSil, base.decorationCharge, base.cateringCharge);
            }
            void GoldPackage()
            {
                string packageGol = "Gold package";
                base.decorationCharge = 15000;
                base.cateringCharge = 180000;
                DisplayDetails(packageGol, base.decorationCharge, base.cateringCharge);
            }
            void PlatinumPackage()
            {
                string packagePla = "Platinum package";
                base.decorationCharge = 25000;
                base.cateringCharge = 300000;
                DisplayDetails(packagePla, base.decorationCharge, base.cateringCharge);
            }
        }
        public override void DisplayDetails(string packageName, double decorationCharge, double cateringCharge)
        {
            if (packageName == "Silver package")
            {
                Console.WriteLine("Congrats, your booking is done!!!!");
                Console.WriteLine("Booking type : Anniversary party");
                Console.WriteLine("Booking date :" + bookingDate);
                Console.WriteLine("Package type :" + packageName);
                Console.WriteLine("Party venue :" + base.venue);
                double totalCost = decorationCharge + cateringCharge;
                Console.WriteLine("Your total bill is :" + totalCost);
                Console.WriteLine("Thanks for booking with us, Enjoy your party!!!");

            }
            else if (packageName == "Gold package")
            {
                Console.WriteLine("Congrats, your booking is done!!!!");
                Console.WriteLine("Booking type : Anniversary party");
                Console.WriteLine("Booking date :" + bookingDate);
                Console.WriteLine("Package type :" + packageName);
                Console.WriteLine("Party venue :" + base.venue);
                double totalCost = decorationCharge + cateringCharge;
                Console.WriteLine("Your total bill is :" + totalCost);
                Console.WriteLine("Thanks for booking with us, Enjoy your party!!!");
            }
            else
            {
                Console.WriteLine("Congrats, your booking is done!!!!");
                Console.WriteLine("Booking type : Anniversary party");
                Console.WriteLine("Booking date :" + bookingDate);
                Console.WriteLine("Package type :" + packageName);
                Console.WriteLine("Party venue :" + base.venue);
                double totalCost = decorationCharge + cateringCharge;
                Console.WriteLine("Your total bill is :" + totalCost);
                Console.WriteLine("Thanks for booking with us, Enjoy your party!!!");
            }

        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            DateTime now = DateTime.Now;
            Random rd = new Random();
            int val;
            int bookingType;
            string cond;
            string date = now.ToString();

            do
            {
                Console.WriteLine("Welcome to party Managment System");
                Console.WriteLine("Select your party type");
                Console.WriteLine("1.Birthday pary");
                Console.WriteLine("2.Anniversary pary");
                bookingType = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine();
                int noOfPeople;
                Birthday bParty = new Birthday();
                Anniversary aParty = new Anniversary();
                switch (bookingType)
                {
                    case 1:
                        Console.WriteLine("Enter no of people coming");
                        noOfPeople = Convert.ToInt32(Console.ReadLine());
                        val = rd.Next(10000, 99999);
                        bParty.BirthdayArrangment(date, noOfPeople, val);
                        break;
                    case 2:
                        Console.WriteLine("Enter no of people coming");
                        noOfPeople = Convert.ToInt32(Console.ReadLine());
                        val = rd.Next(10000, 99999);
                        aParty.AnniversaryArrangment(date, noOfPeople, val);
                        break;
                    default:
                        Console.WriteLine("Entered wrong choice,wanna try agaiin??");
                        break;
                }

                cond = Console.ReadLine();
            } while (cond == "y" || cond == "Y");
        }
    }
}
