 No need to create any TABLE or attaching any output. Just write the queries below each question and forwad that to me.
--------------------------------------------------------------------------------------------------------------------------


1. Write a SQL statement to ADD a column region_id to the TABLE locations.

ALTER TABLE locations 
ADD region_id (varchar 20);

2. Write a SQL statement to ADD a columns ID as the FIRST column of the TABLE locations

ALTER TABLE locations ADD ID int FIRST

3. Write a SQL statement to ADD a column region_id after state_province to the TABLE locations

ALTER TABLE locations ADD region_id varchar(20)  AFTER state_province

4. Write a SQL statement to drop the column city from the TABLE locations.

ALTER TABLE locations  DROP column city;

5. Write a SQL statement to ADD a PRIMARY KEY for the columns location_id in the locations TABLE

ALTER TABLE locations ADD PRIMARY KEY (location_id)