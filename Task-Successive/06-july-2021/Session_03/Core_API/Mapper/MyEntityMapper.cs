﻿using AutoMapper;
using Core_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core_API.Mapper
{
    public class MyEntityMapper : Profile
    {
        public MyEntityMapper()
        {
            CreateMap<Data.Model.Employee, EmployeeModel>();
            CreateMap<EmployeeModel, Data.Model.Employee>();



        }
    }
}
