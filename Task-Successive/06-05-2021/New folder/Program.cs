﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


    class Vehicle
    {
        private string vehicleName;
        string color;
        int noOfWheel;
        const int MAX_SPEED = 1;

    
    // Defailt constructor
    // not needed if no parameterized constructor is available
    public Vehicle() { }
        
    // Constructor with single parameter
        public Vehicle(string vehicleName)
        {
            this.vehicleName = vehicleName;
        }

    // Parametrized constructor
    public Vehicle(string name, string c, int wheel) {
        vehicleName = name;
        color = c;
        noOfWheel = wheel;
    }

        // function with retrun type void (no return values should be accessed)
        public void start() {
            Console.WriteLine("Start");
        }

    // function with return type int
    public int check()
    {
        return 0;
    }

        public void stop() {
            Console.WriteLine("Stop");
        }

        public void speedUp(int speed) {
            Console.WriteLine("Speed increased by "+speed);
        }

}


   
class Program
{
    // Main function
    // Needed in order to run our program
    public static void Main(string[] args)
    {
        Vehicle vehicle = new Vehicle("Honda");
        vehicle.start();
        vehicle.speedUp(10);
        vehicle.stop();
    }
}

//when we create a function static than we can call a particular static function without object//