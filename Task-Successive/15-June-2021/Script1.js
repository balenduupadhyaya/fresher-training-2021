function validate() {
    var firstname = document.regform.firstname.value;
    var lastname = document.regform.lastname.value;
    var rollno = document.regform.rollno.value;
    var email = document.regform.email.value;
    var pass = document.regform.password.value;
    var cpass = document.regform.confirmpassword.value;
    var addrs = document.regform.address.value;
    var mob = document.regform.mobilenumber.value;


    if (firstname == "") {
        document.getElementById('fnameerror').innerHTML = "Please fill FirstName";
        return false;
    }
    if (firstname.length <= 2 || firstname.length >= 40) {
        document.getElementById('fnameerror').innerHTML = "Lenght must be between 2 and 20";
        return false;
    }
    if (!isNaN(firstname)) {
        document.getElementById('fnameerror').innerHTML = "Only characters allowed";
        //single numeric value error
    }

    if (lastname == "") {
        document.getElementById('lname').innerHTML = "Please fill LastName";
        return false;
    }
    if (lastname.length <= 2 || lastname.length >= 40) {
        document.getElementById('lname').innerHTML = "Lenght must be between 2 and 20";
        return false;
    }
    if (!isNaN(lastname)) {
        document.getElementById('lname').innerHTML = "Only characters allowed";
        //single numeric value error
    }

    if (rollno == "") {
        document.getElementById('rno').innerHTML = "Please Fill RollNo";
        return false;
    }

    if (addrs == "") {
        document.getElementById('adds').innerHTML = "Address cant be empty";
        return false;
    }

    if (email=="") {
        document.getElementById('eml').innerHTML = "Email cannot be blank";
        return false;
    }
    if (!email.match("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$")) {
        document.getElementById('eml').innerHTML = "Not a valid email";
        return false;
    }

    if (pass.match("(?=.*[!@#%])(?=.*[a-z])(?=.*[A-Z]).{8,}")) {
        return true;
    }
    else {
        document.getElementById('pas').innerHTML = "Must contain at least one  number and one uppercase and lowercase letter and a special character , and at least 8 or more characters";
        return false;
    }
    if (cpass.match("(?=.*[!@#%])(?=.*[a-z])(?=.*[A-Z]).{8,}")) {
        return true;
    }
    else {
        document.getElementById('cpas').innerHTML = "Must contain at least one  number and one uppercase and lowercase letter and a special character , and at least 8 or more characters";
        return false;
    }
    
    
    if (isNaN(mob)) {
        document.getElementById('mobile').innerHTML = "Cannot be characters";
        return false;
    }
    if (mob.length != 10) {
        document.getElementById('mobile').innerHTML = "Should be of 10 digit";
        return false;
    }





    

    function handleStorage(event) {
        var firstname = document.regform.firstname.value;
        var lastname = document.regform.lastname.value;
        var rollno = document.regform.rollno.value;
        var email = document.regform.email.value;
        var pass = document.regform.password.value;
        var cpass = document.regform.confirmpassword.value;
        var addrs = document.regform.address.value;
        var mob = document.regform.mobilenumber.value;


       

        console.log(firstName + " " + middleName + " " + lastName);

        var data = {
            "firstn ame": firstname,
            "middleName": middlename,
            "lastName": lastname,
            "rollno": rollno,
            "email": email,
            "pass": pass,
            "cpass": cpass,
            "mobile": mob,
            "address": addrs,
        };
        localStorage.setItem(new Date(), JSON.stringify(data));
    }

    if (validateConfirmPassword() == true && validateForm() == true && validatePassword() == true) {

        handleStorage();
    }

    for (var i = 0; i < localStorage.length; i++) {

        var key = localStorage.key(i);
        var value = localStorage.getItem(key);
        var myData = JSON.parse(value);

        document.write("<tr>");
        document.write("<th>", i + 1, "</th>");
        document.write("<td>", myData.firstName, "</td>");
        document.write("<td>", myData.middleName, "</td>");
        document.write("<td>", myData.lastName, "</td>");
        document.write("<td>", myData.email, "</td>");
        document.write("<td>", myData.address, "</td>");
        document.write("<td> <button>", "Delete", " </button> </td>");
        document.write("</tr>");

    function deleteElement(id) {

        localStorage.removeItem(id);

    }
    
   


    
}