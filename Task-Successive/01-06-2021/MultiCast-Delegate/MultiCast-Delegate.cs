﻿using System;

namespace MultiCast_Delegate
{
    public delegate void rectangleDelegate(double Width, double Height);
    class Program
    {
        //public delegate void perimeterDelegate(double Width, double Height);
        //No need 1 deligate can call many methods but signature must be same
        public void GetArea(double Width,double Height) //Pascal Case 
        {
            Console.WriteLine("Area is "+Width * Height);
        }
        //if we ruturn a value they get overwrite  --- return value store krani padati --In multicast ignore return value methods
        public void GetPerimeter(double Width,double Height)
        {
            Console.WriteLine("Perimeter is "+2 * (Width + Height));
        }
        static void Main(string[] args)
        {
            Program rectangle = new Program();
            Console.WriteLine("Enter the Height ");
            double Width = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Enter the Width ");
            double Height = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine();
            Console.WriteLine("Without Deligate ");

            rectangle.GetArea(Width, Height);
            rectangle.GetPerimeter(Width, Height);

            Console.WriteLine("With Delegate");
            Program rectDeli = new Program();
            rectangleDelegate obj = rectDeli.GetArea;
            obj(Width, Height);
            obj += rectDeli.GetPerimeter;  //MultiCast Delegate   Not Running why ??
            //
            //Console.WriteLine("Passing another values in delegate object ");
            //area(10, 30);
        }
    }
}
