﻿using System;

namespace _01_06_2021
{
    //Defining a Delegate ----- Defined Under Namaspace --Can be Possible under Class
    public delegate void additionDelegate(double NumOne, double NumTwo); //Delegate defined outside the class
    class Delegate
    {
        public delegate double subtractionDelegate(double NumOne, double NumTwo); //Delegate defined inside the class


        public static void Add(double NumOne, double NumTwo)  // Add Function Static and Void Return Type
        {

            Console.WriteLine("Sum is " + (NumOne + NumTwo));
        }
        public double  Subtract(double NumOne,double NumTwo)  //Subtract Function non Static and having return 
        {
            double sub = NumOne - NumTwo;
            return  sub;
        }
        static void Main(string[] args)
        {
            double sub; //Global varible Can be use mutiple times and it is defined outside the function 
                        //If a variable is defined inside the function it is known as local Variable
           
            Console.WriteLine("Enter First Number");
            double numOne = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine();

            Console.WriteLine("Enter Second Number");
            double numTwo = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine();

           

            Delegate obj = new Delegate();            //NonStatic Funtion invoke with intantiating with obj
             

            

            //Instantiating a delegate
            Delegate p = new Delegate();          //For Non-static method we have to use object of class 
                                                   //For static method we can use it directly

            additionDelegate ad = new additionDelegate(Add); //Invoke without object of class ..It wil show error if we try
            subtractionDelegate sd = new subtractionDelegate(p.Subtract);




            Console.WriteLine("Choose one \n 1 for Delegate \n 2 for Non-Delegate ");
            int num = Convert.ToInt32(Console.ReadLine());

            switch (num)
            {
            case 1:
                    //Calling the delegate
                    Console.WriteLine();
                    Console.WriteLine("With use of Delegate ");
                    Console.WriteLine();
                    ad(numOne, numTwo); // or it can be invoke by ad.Invoke(numOne,numTwo);
                    sub = sd(numOne, numTwo);
                    Console.WriteLine("Subtraction is " + sub);
                    break;
                    

                case 2:
                    Console.WriteLine();
                    Console.WriteLine("Without Use of Delegate ");
                    Console.WriteLine();
                    Add(numOne, numTwo);                    //Static function invoke without object instantiating
                    Console.WriteLine();
                    sub = obj.Subtract(numOne, numTwo);   //Return value is stored in a sub variable
                    Console.WriteLine("Subtraction is " + sub);
                    Console.WriteLine();
                    break;
                default:
                    Console.WriteLine("Choose Correct Option");
                    break;

            }
        }
    }
}
