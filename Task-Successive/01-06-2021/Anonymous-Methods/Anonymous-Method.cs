﻿using System;

namespace Anonymous_Methods
{
    class Program
    {
        public delegate string Greetings(string Name);
        static void Main(string[] args)
        {
            //in Anonymous methods we dont have to crete a fully type methods,just simply follow the process
            Program obj = new Program();
            Greetings greet = delegate (string name)  //anonymous methods 
              {
                  return "Hello " + name;
              };// Use Semicolon in anonymous methods
            string str = greet.Invoke("Balendu");
            Console.WriteLine(str);
        }
    }
}
