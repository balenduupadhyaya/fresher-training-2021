﻿using System;

namespace Lambda_Expression
{
    public delegate string GreetingsDelegate(string Name);
    class Program
    {
        // Method
        //public  string Greetings(string Name)
        //{
        //    return "Hello,How are you" + Name;
        //}
        static void Main(string[] args)
        {
            GreetingsDelegate obj = (Name) =>
            {
                return "Hello , MR " + Name; //Since this is a return type Delegate we have to store it in a variable and than print it
            };
            string str = obj.Invoke("Balendu");
            Console.WriteLine(str);
        }
    }
}
