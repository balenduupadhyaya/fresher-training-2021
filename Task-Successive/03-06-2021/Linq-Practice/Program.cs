﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Linq_Practice;

namespace Linq_Practice
{
    public class List1
    {
        public string EmployeeName { get; set; }
        public string EmployeeDepartment;


    }
    public class List2
    {
        public int StudentId;
        public string StudentName;
        public int Age;
    }
    class Program
    {
        static void Main(string[] args)
        {
            List<List1> l = new List<List1>();
            {
                l.Add(new List1() { EmployeeName = "Abhishek", EmployeeDepartment = "Hr" });
                l.Add(new List1() { EmployeeName = "Aditya", EmployeeDepartment = "Hr" });
                l.Add(new List1() { EmployeeName = "Bhushan", EmployeeDepartment = "Director" });
                l.Add(new List1() { EmployeeName = "Manoj", EmployeeDepartment = "Sales" });
                l.Add(new List1() { EmployeeName = "Harish", EmployeeDepartment = "Engineer" });

            };

            int result = l.Count(s => s.EmployeeName.Equals("Abhishek"));
            Console.WriteLine("Result=" + result);

            var result2 = l.Where(a => a.EmployeeName.Contains("A"));
            foreach (var r in result2)
            {
                Console.WriteLine("Name that contains a are " + r);

            }

            List<List2> lTwo = new List<List2>();
            {
                new List2() { StudentName = "Ram", StudentId = 1, Age = 10 };
                new List2() { StudentName = "Sita", StudentId = 2, Age = 20 };
                new List2() { StudentName = "Ravan", StudentId = 3, Age = 30 };
            };
            var res = l.GroupJoin(lTwo,
                std => std.EmployeeName,
                s => s.StudentName,
                (std, studentsGroup) => new
                {
                    Students = studentsGroup,
                    FullName = std.EmployeeName              //Doubt in Group Join Concept
                });

            foreach(var item in res)
            {
                Console.WriteLine(item.FullName);
                foreach(var stud in item.Students)
                {
                    Console.WriteLine(stud.StudentId);
                }
            }

    }
    }
}
