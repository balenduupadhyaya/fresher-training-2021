﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Web_Application.Filter
{
    public class CustomeActionFilter : IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext context)
        {
            var routedata = context.RouteData;
            var action = routedata.Values["action"];
            var controller = routedata.Values["controller"];
            Debug.Write(string.Format("Controller is :{0} and the Action used is :{1}", controller, action), "Log action from Action Filter OnActionExecutedis : ");
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            var routedata = context.RouteData;
            var action = routedata.Values["action"];
            var controller = routedata.Values["controller"];
            Debug.Write(string.Format("Controller is :{0} and the Action used is :{1}", controller, action), "Log action from Action Filter OnActionExecuting is : ");
        }
    }
}
