﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Web_Application.Filter
{
    //In this we can define the order and this is done by class
    //asnyc mai execution agge bd jata hai method parallely execute hote rhte hai
    public class CustomeActionFilterAttribute : ActionFilterAttribute
    {
        public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next) 
        {
            var routedata = context.RouteData;
            var action = routedata.Values["action"];
            var controller = routedata.Values["controller"];
            Debug.Write(string.Format("Controller is :{0} and the Action used is :{1}", controller, action), "Log action from Action Filter OnActionExecuting is : ");
            _ = await next(); // Either mark it complete or next
        
                }

    }
}
