﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Web_Application.Filter
{
    public class ResultFilter : IResultFilter
    {
        public void OnResultExecuted(ResultExecutedContext context)
        {
            var routedata = context.RouteData;
            var action = routedata.Values["action"];
            var controller = routedata.Values["controller"];
            Debug.Write(string.Format("Controller is :{0} and the Action used is :{1}", controller, action), "Log action from Result Filter OnAExecuted is : ");
        }

        public void OnResultExecuting(ResultExecutingContext context)
        {
            var routedata = context.RouteData;
            var action = routedata.Values["action"];
            var controller = routedata.Values["controller"];
            Debug.Write(string.Format("Controller is :{0} and the Action used is :{1}", controller, action), "Log action from Result Filter OnAExecution is : ");
        }
    }
}
