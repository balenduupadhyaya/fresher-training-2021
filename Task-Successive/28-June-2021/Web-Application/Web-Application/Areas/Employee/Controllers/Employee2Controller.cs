﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web_Application.Filter;
using Web_Application.Models.Employee;

namespace Web_Application.Areas.Employee.Controllers
{
    [Area("Employee")]
    public class Employee2Controller : Controller
    {
        // GET: EmployeeControllerWithResultFilter
       [ServiceFilter(typeof(ResultFilter))]
        public ActionResult Index()
        {
            return View("Employee2", new EmployeeViewModel());
        }
        public ActionResult Employee2(EmployeeViewModel employee)
        {
            ViewBag.EmployeeId = employee.EmployeeId;
            ViewBag.EmployeeName = employee.EmployeeName;
            ViewBag.EmployeeAge = employee.EmployeeAge;
            return View("Employee2", employee);
        }
    }
}
