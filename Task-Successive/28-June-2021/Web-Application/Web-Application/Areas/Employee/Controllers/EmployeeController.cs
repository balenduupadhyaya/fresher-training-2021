﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web_Application.Models.Employee;

namespace Web_Application.Areas.Employee.Controllers
{
    [Area("Employee")]
    public class EmployeeController : Controller
    {
        // GET: EmployeeController
        public ActionResult Index()
        {
            return View("Employee", new EmployeeViewModel());
        }

        // GET: EmployeeController/Details/5
        public ActionResult Employee(EmployeeViewModel employee)
        {

            ViewBag.EmployeeId = employee.EmployeeId;
            ViewBag.EmployeeName = employee.EmployeeName;
            ViewBag.EmployeeAge = employee.EmployeeAge;
            return View("Employee", employee);
        }

    }
}