---------------------Examples Of Trigers------------


CREATE TRIGGER TriggerOne
on WORKERS
AFTER INSERT
as

begin
print'Cannot Insert'
rollback transaction
end

Select * from WORKERS

INSERT INTO Workers 
	(WORKER_ID, FIRST_NAME, LAST_NAME, SALARY, DEPARTMENT) VALUES
		(009, 'Monika', 'Sharma', 170000, 'HR');
		go



create trigger TriggerTwo
on workers
FOR Insert
as

begin
print' Insert'
rollback transaction
end

Select * from WORKERS
drop trigger TriggerTwo
drop trigger TriggerOne


INSERT INTO Workers 
	(WORKER_ID, FIRST_NAME, LAST_NAME, SALARY, DEPARTMENT) VALUES
		(010, 'Shivam', 'Sharma', 196000, 'HR')go



Alter trigger TriggerTwo
on workers
FOR Update
as

begin
print' Insert'
rollback transaction
end

Select * from WORKERS
Update Workers set First_Name='Rahul' where WORKER_ID=6
go

Alter trigger TriggerTwo
on workers
After Update
as

begin
print' No After'
rollback transaction
end

Select * from WORKERS
Update Workers set First_Name='Rahul' where WORKER_ID=6


----------------Group By---------------

select * from Workers
select count(LAST_NAME) as LName from Workers group by DEPARTMENT 
Select salary from Workers group by SALARY
Select LAST_NAME from Workers group by SALARY