﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models
{
    public class EmployeeModel
    {
       
            public int EmployeeId { get; set; }

            public string EmployeeName { get; set; }

            public int EmployeeAge { get; set; }

            public string Gender { get; set; }

            public Int64 MobileNumber { get; set; }
        
    }
}
