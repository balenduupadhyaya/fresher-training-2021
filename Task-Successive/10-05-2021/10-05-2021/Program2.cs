﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10_05_2021
{
    public abstract  class Vehicle
    {
        // private string vehicleName;
        public string color;
        public int noOfWheels;
        public readonly int maxSpeed;  //Max speed is changed to readonly from constant 
        public Vehicle(string clr, int wheel, int speed)
        {

            color = clr;
            noOfWheels = wheel;
            maxSpeed = speed;
        }

        public abstract int callTollAmount();
    }

    sealed class Car : Vehicle //sealed is used to make class ensure that it can't be further inherited
    {
        int weight;// = Convert.ToInt32(Console.ReadLine());

        public Car(string clr, int wheel, int speed, int weight)
            : base(clr, wheel, speed) // How to call constructor of parent/base class from child/derived class
                                      //firstly parent class construcor is called than child class constructor is called
        {
            this.weight = weight; // if the name in parameter is same than we use this keyword.
        }

        public override string ToString() //  to string ka fyada hai ki automatically call hota hai print ke lie
        {
            return $"Color: {color}\nWheels: {noOfWheels}\nSpeed: {maxSpeed}\nWeight: {weight}";
        }

        //public string Details()
        //{
        //    return "";                           -------->     iska kamm tostring function ne kr dia
        //}

        public override int callTollAmount()
        {
            return weight > 1000 ? 1000 : 500;
        }
    }

    sealed class Bike : Vehicle
    {
        int weight;
        public Bike(string clr, int wheel, int speed, int weight)
            : base(clr, wheel, speed) // How to call constructor of parent/base class from child/derived class
                                      //firstly parent class construcor is called than child class constructor is called
        {
            this.weight = weight;
        }
        public override string ToString()
        {
            return $"Color: {color}\nWheels: {noOfWheels}\nSpeed: {maxSpeed}\nWeight: {weight}";
        }
        public override int callTollAmount()
        {
            return 200;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Car a = new Car("white", 4, 100, 1500);
            // Console.WriteLine(a.callTollAmount()); // jb tak function static no ho vo object se call hote hai
            Console.WriteLine("Car");
            Console.WriteLine(a);
            Bike b = new Bike("Black", 2, 70, 70);
            Console.WriteLine("Bike");
            Console.WriteLine(b);

        }

    }
}
