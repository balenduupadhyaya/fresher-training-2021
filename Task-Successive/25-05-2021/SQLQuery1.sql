CREATE DATABASE Company; 

CREATE TABLE Employee(
EmpId INT identity(101,1) Primary Key,
EmpName varchar(20),
Department varchar(20),
ContactNo varchar(20),
EmailId varchar(20) ,
EmpHeadId int)


Select * from Employee

INSERT INTO Employee(EmpName,Department,ContactNo,EmailId,EmpHeadId)values ('Isha','E-101','1234567890','isha@gmail.com',105)
INSERT INTO Employee(EmpName,Department,ContactNo,EmailId,EmpHeadId)values ('Priya','E-104','1234567890','priya@gmail.com',105)
INSERT INTO Employee(EmpName,Department,ContactNo,EmailId,EmpHeadId)values ('Neha','E-101','1234567890','neha@gmail.com',105)
INSERT INTO Employee(EmpName,Department,ContactNo,EmailId,EmpHeadId)values ('Rahul','E-102','1234567890','rahul@gmail.com',105)
INSERT INTO Employee(EmpName,Department,ContactNo,EmailId,EmpHeadId)values ('Abhishek','E-101','1234567890','abhishek@gmail.com',105)



Select * from Employee





Create table EmpDept(
DeptId varchar(10) primary key,
DeptName varchar(20),
Dept_off varchar(20),
DeptHead int);

select * from EmpDept

insert into EmpDept(DeptId,DeptName,Dept_off,DeptHead) values ('E-101','HR','Monday',105)
insert into EmpDept(DeptId,DeptName,Dept_off,DeptHead) values ('E-102','Development','Tuesday',101)
insert into EmpDept(DeptId,DeptName,Dept_off,DeptHead) values ('E-103','Hous Keeping','Saturday',103)
insert into EmpDept(DeptId,DeptName,Dept_off,DeptHead) values ('E-104','Sales','Sunday',104)
insert into EmpDept(DeptId,DeptName,Dept_off,DeptHead) values ('E-105','Purchage','Tuesday',104)

select * from EmpDept

Create table EmpSalary(
EmpId varchar(10) primary key,
Salary int,
IsPermanent varchar(20))

select * from EmpSalary

insert into EmpSalary(EmpId,Salary,IsPermanent) values('101',2000,'Yes')
insert into EmpSalary(EmpId,Salary,IsPermanent) values('102',10000,'Yes')
insert into EmpSalary(EmpId,Salary,IsPermanent) values('103',5000,'No')
insert into EmpSalary(EmpId,Salary,IsPermanent) values('104',1900,'Yes')
insert into EmpSalary(EmpId,Salary,IsPermanent) values('105',2300,'Yes')

select * from EmpSalary

Create table Project(
ProjectId nvarchar(10) primary key,
Duration int)

Select * from Project

Insert into Project(ProjectId,Duration) values('p-1',23)
Insert into Project(ProjectId,Duration) values('p-2',15)
Insert into Project(ProjectId,Duration) values('p-3',45)
Insert into Project(ProjectId,Duration) values('p-4',2)
Insert into Project(ProjectId,Duration) values('p-5',30)

Select * from Project

Create table Country(
cid nvarchar(10) primary key,
cname Varchar(20))

Select * from Country

Insert into Country(cid,cname) values ('c-1','India')
Insert into Country(cid,cname) values ('c-2','Usa')
Insert into Country(cid,cname) values ('c-3','China')
Insert into Country(cid,cname) values ('c-4','Pakistan')
Insert into Country(cid,cname) values ('c-5','Russia')

Select * from Country

Create table ClientTable(
ClientId nvarchar(10) primary key,
ClientName varchar(20),
cid nvarchar(20))

Select * from ClientTable

Insert into ClientTable(ClientId,ClientName,cid) values ('c1-1','ABC Group','c-1')
Insert into ClientTable(ClientId,ClientName,cid) values ('c1-2','PQR','c-1')
Insert into ClientTable(ClientId,ClientName,cid) values ('c1-3','XYZ','c-2')
Insert into ClientTable(ClientId,ClientName,cid) values ('c1-4','tech altum','c-3')
Insert into ClientTable(ClientId,ClientName,cid) values ('c1-5','mnp','c-5')

Select * from ClientTable




create table EmpProject(
EmpId int identity(101,1),
ProjectId nvarchar(10),
ClientId nvarchar(10),
StartYear int,
EndYear int)

Select * from EmpProject

insert into EmpProject(ProjectId,ClientId,StartYear,EndYear) values('p-1','C1-1',2010,2010)
insert into EmpProject(ProjectId,ClientId,StartYear,EndYear) values('p-2','C1-2',2010,2012)
insert into EmpProject(ProjectId,ClientId,StartYear,EndYear) values('p-1','C1-3',2010,null)
insert into EmpProject(ProjectId,ClientId,StartYear,EndYear) values('p-4','C1-1',2010,2010)
insert into EmpProject(ProjectId,ClientId,StartYear,EndYear) values('p-4','C1-5',2010,null)

Select * from EmpProject

--Answer-1
select EmpName as EmployeeTable from Employee;
--Answer-2
select * from Employee where EmpName like 'P%'
--Answer-3
select * from Employee where EmailId like '%@gmail.com'
--Answer-4
Select * from Employee where Department ='E-104' or Department='E-102'
--Answer-5
Select COUNT (isPermanent)
from EmpSalary
where Salary > 5000;
--Answer-6
select DeptName from EmpDept where DeptId='E-102'


