﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models
{
    public class Student
    {
        [Required]
        public string name { get; set; }
        [Required]
        public int rollno { get; set; }
        [Required]
        public string address { get; set; }
        [Required]
        public string email { get; set; }

            
    }
}
