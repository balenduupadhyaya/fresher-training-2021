﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.Models;
using WebApplication.Repositories;

namespace WebApplication.Controllers
{
    public class StudentController : Controller
    {
        private readonly ILogger<StudentController> _logger;
        private readonly IConfiguration _configuration;
        private readonly IStudentRepository _studentRepository;
        public StudentController(ILogger<StudentController> logger, IConfiguration configuration , IStudentRepository studentRepository)
        {
            _logger = logger;
            _configuration = configuration;
            _studentRepository = studentRepository;
        }

        public IActionResult List()
        {
            
            var list = new List<Student> { new Student() { name = "Vishal",rollno=1,address="Ghaziabad",email="Vishal@gmail.com" },
             new Student() { name = "Pawan",rollno=2,address="Noida",email="Pawan@gmail.com" }};
            ViewBag.Client = _configuration.GetValue<string>("client");
            return View("List", list);

        }

        public IActionResult Privacy(Student student)
        {
            if (!ModelState.IsValid)
            {
                return View("list");
            }
            var list = new List<Student> { new Student() { name = "Vishal",rollno=1,address="Ghaziabad",email="Vishal@gmail.com" },
             new Student() { name = "Pawan",rollno=2,address="Noida",email="Pawan@gmail.com" }};
            return View("list");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
