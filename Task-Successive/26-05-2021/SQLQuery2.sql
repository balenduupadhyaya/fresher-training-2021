Create Database WorkersManagement;

CREATE TABLE Workers (
	WORKER_ID INT NOT NULL  PRIMARY KEY ,
	FIRST_NAME CHAR(25),
	LAST_NAME CHAR(25),
	SALARY INT,
	JOINING_DATE DATETIME,
	DEPARTMENT CHAR(25)
);
Select * from Workers;

INSERT INTO Workers 
	(WORKER_ID, FIRST_NAME, LAST_NAME, SALARY, DEPARTMENT) VALUES
		(001, 'Monika', 'Arora', 100000, 'HR'),
		(002, 'Niharika', 'Verma', 80000, 'Admin'),
		(003, 'Vishal', 'Singhal', 300000, 'HR'),
		(004, 'Amitabh', 'Singh', 500000, 'Admin'),
		(005, 'Vivek', 'Bhati', 500000, 'Admin'),
		(006, 'Vipul', 'Diwan', 200000,  'Account'),
		(007, 'Satish', 'Kumar', 75000,  'Account'),
		(008, 'Geetika', 'Chauhan', 90000, 'Admin');

CREATE TABLE Bonus (
	WORKER_REF_ID INT,
	BONUS_AMOUNT INT,
	BONUS_DATE DATETIME,
	FOREIGN KEY (WORKER_REF_ID)
		REFERENCES Workers(WORKER_ID)
        ON DELETE CASCADE
);

select * from Bonus


INSERT INTO Bonus 
	(WORKER_REF_ID, BONUS_AMOUNT) VALUES
		(001, 5000),
		(002, 3000),
		(003, 4000),
		(001, 4500),
		(002, 3500);

CREATE TABLE Title (
	WORKER_REF_ID INT,
	WORKER_TITLE CHAR(25),
	AFFECTED_FROM DATETIME,
	FOREIGN KEY (WORKER_REF_ID)
		REFERENCES Workers(WORKER_ID)
        ON DELETE CASCADE
);

INSERT INTO Title 
	(WORKER_REF_ID, WORKER_TITLE, AFFECTED_FROM) VALUES
 (001, 'Manager', '2016-02-20 00:00:00'),
 (002, 'Executive', '2016-06-11 00:00:00'),
 (008, 'Executive', '2016-06-11 00:00:00'),
 (005, 'Manager', '2016-06-11 00:00:00'),
 (004, 'Asst. Manager', '2016-06-11 00:00:00'),
 (007, 'Executive', '2016-06-11 00:00:00'),
 (006, 'Lead', '2016-06-11 00:00:00'),
 (003, 'Lead', '2016-06-11 00:00:00');

 select * from Title;
 /*
 Q1. select all Worker whose First_Name's 2nd charactor is 'i'.
Q2. Write an SQL query that fetches the unique values of DEPARTMENT from Worker table.
Q3. Write an SQL query to print all Worker details from the Worker table order by FIRST_NAME Ascending.
Q4. Write an SQL query to print all Worker details from the Worker table order by FIRST_NAME Ascending and DEPARTMENT Descending.
Q5. Write an SQL query to print details for Workers with the first name as �Vipul� and �Satish� from Worker table.
Q6. Select * from Worker where FIRST_NAME not in ('Vipul','Satish');
Q7. Write an SQL query to print details of Workers with DEPARTMENT name as �Admin�.
Q8. Write an SQL query to print details of the Workers whose FIRST_NAME contains �a�.
Q9. Write an SQL query to print details of the Workers whose FIRST_NAME ends with �h� and contains six alphabets.
Q10. Select * from Worker where SALARY between 100000 and 500000;
Q11. Write an SQL query to fetch worker names with salaries >= 50000 and <= 100000.
Q12. Write an SQL query to print details of the Workers who are also Managers.
Q13. Get worker with Bonus Amount > 3500.
Q14. Get all the workers with their Bonus details.

 */
 --Answer--1
 Select * from Workers where FIRST_NAME Like  '_i%'
 --Answer--2
 Select Distinct DEPARTMENT from Workers order by DEPARTMENT
 --Answer--3
 Select * from Workers order by FIRST_NAME asc
 --Answer--4
  Select * from Workers order by FIRST_NAME asc ,DEPARTMENT desc
 --Answer--5
 Select * from Workers where FIRST_NAME='Vipul' or FIRST_NAME='Satish'
 --Answer--6
 Select* from Workers where FIRST_NAME not in('Vipul','Satish')
 --Answer--7
 Select * from Workers where DEPARTMENT='Admin'
 --Answer--8
 select * from Workers where FIRST_NAME like '%a%'
 --Answer--9
 Select * from Workers where FIRST_NAME like '_____h'
 --Answer--10
 Select * from Workers where SALARY between 100000 and 500000;
 --Answer--11
 select FIRST_NAME , LAST_NAME from Workers where SALARY >= 50000 and SALARY<= 100000 
 --Answer--12
 Select * from Workers inner join Title on Workers.WORKER_ID=Title.WORKER_REF_ID and Title.WORKER_TITLE='Manager'
 --Answer--13
 Select * from Workers inner join Bonus on BONUS_AMOUNT >3500 and WORKER_ID=WORKER_REF_ID
 --Answer--14
 select * from workers left join Bonus on WORKER_ID=WORKER_REF_ID


 