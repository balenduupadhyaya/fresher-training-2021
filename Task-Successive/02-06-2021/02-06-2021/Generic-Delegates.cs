﻿using System;

namespace _02_06_2021
{
    class Program
    {
        public static double Addition(int Value1,float Value2,double Value3)  //we can use func generic delegate for this
        {
            return Value1 + Value2 + Value3;
        }
        public static void Subtract(int Value1,float Value2,double Value3) // We can use action generic delegate here
        {
            Console.WriteLine("Subtraction is "+(Value1 - Value2 - Value3));
        }
        public static bool CheckLength(string str)    //Here we use predicate delegate
        {
            if (str.Length > 5)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        static void Main(string[] args)
        {

            //Delegate mai object se hi hoga kaam apna object create krna pdega 
            Func<int, float, double, double> obj = Addition;   //here we use func delegate 3 Input Parameters and one output //And sirf data type batane hai andr
            double result = obj.Invoke(100, 50.2f, 300.102);
            Console.WriteLine("" +
                "Addition of Values are " + result);

            Action<int, float, double>obj2 = Subtract;
            obj2.Invoke(30, 10.2f, 5.32);

            Predicate<string> obj3 = CheckLength;
            bool status = obj3.Invoke("Balendu");
            Console.WriteLine(status);
        }
    }
}
