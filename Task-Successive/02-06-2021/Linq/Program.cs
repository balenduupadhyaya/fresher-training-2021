﻿using System;
using System.Collections.Generic;
using System.Linq;
using Linq;

namespace Linq
{
    public class Employee
    {
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public Double Salary { get; set; }
        public Double Experience { get; set; }

        public string Location { get; set; }

    }
    class Program
    {
        static void Main(string[] args)
        {
            List<Employee> emp = new List<Employee>();
            { emp.Add(new Employee() { EmployeeId = 1, EmployeeName = "Balendu", Experience = 2.5, Location = "Noida", Salary = 10203.00 });
                emp.Add(new Employee() { EmployeeId = 2, EmployeeName = "Abhishek", Experience = 1.5, Location = "Noida", Salary = 20203.00 });
                emp.Add(new Employee() { EmployeeId = 3, EmployeeName = "Ram", Experience = 3.5, Location = "Pune", Salary = 30203.00 });
                emp.Add(new Employee() { EmployeeId = 4, EmployeeName = "Sita", Experience = 0.5, Location = "Pune", Salary = 10203.1 });
                emp.Add(new Employee() { EmployeeId = 5, EmployeeName = "Hanuman", Experience = 2, Location = "Noida", Salary = 20000 });
                emp.Add(new Employee() { EmployeeId = 6, EmployeeName = "Ravan", Experience = 1, Location = "Noida", Salary = 20325.20 });
            };
            //Linq Qwery Methos here
            var employee = emp.Where(e => e.EmployeeId > 3 && e.Experience > .5);
            Console.WriteLine();
           
            foreach (Employee employ in employee)
            {
                Console.WriteLine(employ.EmployeeName);
            }
            //Sum Method
            var totalSalary = emp.Sum(s => s.Salary);
            Console.WriteLine("Total Salary :{0}", totalSalary);

            var salary = emp.Count(sal => sal.Salary > 10000);
            Console.WriteLine("Salary lelo "+salary);

            //var loc = emp.Where(l => l.Location = "Noida");
            ////Console.WriteLine(loc);
            //foreach(Employee locc in loc)
            //{
            //    Console.WriteLine(locc.Location);
            //}
            // Sir se Puchna hai Location ka lie kiu ni chal rha hai

            //Average
            var exp = emp.Average(a => a.Experience);
            Console.WriteLine(exp);


            //Using Qwery Syntax

            var emp1 = from em in emp
                       where em.Experience > 1
                       select em;
            foreach(var empp in emp1)
            {
                Console.WriteLine(empp.EmployeeName);
            }




        }
        
        
    }
}
