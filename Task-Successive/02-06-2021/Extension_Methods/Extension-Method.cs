﻿using System;

namespace Extension_Methods
{
    class Program
    {
        public void TestOne()
        {
            Console.WriteLine("Method 1");
        }
        public void TestTwo()
        {
            Console.WriteLine("Method 2");
        }
        static void Main(string[] args)
        {
            Program obj = new Program();
            obj.TestOne();
            obj.TestTwo();
        }
    }
}
