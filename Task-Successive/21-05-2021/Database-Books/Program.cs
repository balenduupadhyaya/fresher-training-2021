﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2_may_21
{
	struct book
	{
		public string Title;
		public string Author;
	}
	public class Struct
	{
		public static void Main()
		{
			
			book[] books = new book[1000];
			int i, j, k = 1;
			Console.Write("\n\n No of Books :\n");
			int n = int.Parse(Console.ReadLine());
			Console.Write("\n\nEnter the information of books :\n");

			for (j = 0; j < n; j++)
			{
				Console.WriteLine("Information of book {0} :", k);

				Console.Write("ENter name of the book : ");
				books[j].Title = Console.ReadLine();

				Console.Write("Enter the author : ");
				books[j].Author = Console.ReadLine();
				k++;
				Console.WriteLine();
			}

			for (i = 0; i < n; i++)
			{
				Console.WriteLine("{0}: Title = {1},  Author = {2}", i + 1, books[i].Title, books[i].Author);
				Console.WriteLine();

			}
			Console.ReadKey();

		}
	}

}