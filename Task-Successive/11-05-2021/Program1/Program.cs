﻿/*In the "Vehicle" class, change the no of wheels to be of enum type.
Create and interface called IPaiter with Paint mothod in it and implement it in the vehicle class
Create another interface for changing the seat cover and implement it abstraclty in child classes
*/





 using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;          //Enum  wheels vale ka use kha hoga aur kese hoga

namespace _10_05_2021
{
    public enum noOfWheels { Two, Four }  //enum mai convert kr dia hai
    public class Vehicle : IPaint
    {
        public void paint()
        {                                                                   //Interface vala kamm hamne jo vehicle class bana rkhi thi usi mai hoga 
            Console.WriteLine("Bhaiya Chate kia ho ?");
        }
        // private string vehicleName;
        public string color;
        
        public readonly int maxSpeed;  //Max speed is changed to readonly from constant 
        public Vehicle(string clr, int speed)
        {
            
            color = clr;
            maxSpeed = speed;
        }
    }
    public interface IPaint  //// In this we have created a public interface named iPainter with a paint method in it and we are going to implement it in vehicle class
    {
        void paint();
    }
    public interface seatCover
    {
        void seatCoverChanged();
    }
    


    sealed class Car : Vehicle ,seatCover//sealed is used to make class ensure that it can't be further inherited // seat cover is interface 
    {
        public void seatCoverChanged()
        {
            Console.WriteLine("mahange hai seat cover ");
        }
        int weight;// = Convert.ToInt32(Console.ReadLine());

        public Car(string clr, int wheel, int speed, int weight)
            :base(clr,speed) // How to call constructor of parent/base class from child/derived class
            //firstly parent class construcor is called than child class constructor is called
        {
            this.weight = weight; // if the name in parameter is same than we use this keyword.
        }

        public override string ToString() //  to string ka fyada hai ki automatically call hota hai print ke lie
        {
                return $"Color: {color}\nSpeed: {maxSpeed}\nWeight: {weight}";
            }

        //public string Details()
        //{
        //    return "";                           -------->     iska kamm tostring function ne kr dia
        //}

        public int callTollAmount()
        {
                return weight > 1000 ? 1000 : 500;
            }
    }

    sealed class Bike : Vehicle
    {
        int weight;
        public Bike(string clr, int wheel, int speed, int weight)
            : base(clr, speed) // How to call constructor of parent/base class from child/derived class
                                      //firstly parent class construcor is called than child class constructor is called
        {
            this.weight = weight;
        }
        public override string ToString()
        {
                return $"Color: {color}\nSpeed: {maxSpeed}\nWeight: {weight}";
            }
        public int callTollAmount()
        {
                return 200;
            }
    }

    class Program
    {
       static void Main(string[] args)
        {
        Car a = new Car("white", 4, 100, 1500);
                    // Console.WriteLine(a.callTollAmount()); // jb tak function static no ho vo object se call hote hai
        Console.WriteLine("Car");
        Console.WriteLine(a);
        Bike b = new Bike("Black", 2, 70, 70);
        int w = (int)noOfWheels.Two;
        //Console.WriteLine("Weight :{ 0}", w);
            Console.ReadKey();

        Console.WriteLine("Bike");
        Console.WriteLine(b);
        int j = (int)noOfWheels.Two;
        //Console.WriteLine("Weight :{ 1}", j); 
        Console.ReadKey();


            //IPaint d;
            //d = new Vehicle();
            //d.paint();                               yeh hame call krna hai kia ??????

        
                }
    
        }
}