﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Program_2
{
    public class Person
    {
        public static void Main(string[] args)
        {
            Dictionary<string, int> Persons = new Dictionary<string, int>();
            Persons.Add("Person_1", 70);
            Persons.Add("Person_2", 40);
            Persons.Add("Person_3", 30);
            Persons.Add("Person_4", 90);
            Persons.Add("Person_5", 60);

            foreach (KeyValuePair<string, int> data in Persons)
            {
                if (data.Value > 60)
                {
                    Console.WriteLine($"{ data.Key} age is { data.Value}" );
                   
                }

            }
            Console.ReadLine();
        }

    }
}
