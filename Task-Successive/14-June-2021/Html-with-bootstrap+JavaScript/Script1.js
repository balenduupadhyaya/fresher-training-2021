// JavaScript source code
function validate() {
	var firstname = document.form.firstname.value;

	var lastname = document.form.lastname.value;
	var mobile = document.form.contact.value;
	var email = document.form.email.value;
	var address = document.form.address.value;
	var password = document.form.password.value;
	var cpassword = document.form.cpassword.value;


	var pwd_expression = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-])/;
	var letters = /^[A-Za-z]+$/;
	var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	var add = /^[a-zA-Z0-9\s,. '-]{3,}$/;
	var mob = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;


	if (firstname == '') {
		alert('Please enter your name');
	}
	else if (!letters.test(firstname)) {
		alert('Name field required only alphabet characters');
	}
	else if (lastname == '') {
		alert('Please enter your last name');
	}
	else if (!letters.test(lastname)) {
		alert('Name field required only alphabet characters');
	}
	else if (email == '') {
		alert('Please enter your email id');
	}
	else if (!filter.test(email)) {
		alert('Invalid email');
	}
	else if (password == '') {
		alert('Please enter Password');
	}
	else if (cpassword == '') {
		alert('Please Enter Confirm Password');
	}
	else if (!pwd_expression.test(password)) {
		alert('Upper case, Lower case, Special character and Numeric letter are required in Password filed');
	}
	else if (password != cpassword) {
		alert('Password not Matched');
	}
	else if (mobile == '') {
		alert('Please enter mobile number');
	}
	else if (!mob.test(mobile)) {
		alert('follow the pattern 123-456-7891');
	}
	else if (address == '') {
		alert('Please enter your address');
	}
	else if (!add.test(address)) {
		alert('Enter your full address');
	}
	else if (document.form.password.valuelength < 6) {
		alert('Password minimum length is 6');
	}
	else if (document.form.password.value.length > 12) {
		alert('Password maximum length is 12');
	}

	else {
		alert('Registration Succesful ');
	}
}
	
