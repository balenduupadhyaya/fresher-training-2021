﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models
{
    public class Student
    {
        public string name { get; set; }
        public int rollno { get; set; }
        public string address { get; set; }
        public string email { get; set; }
    }
}
