﻿using System.Collections.Generic;

namespace WebApplication.Models
{
    public interface IStudentRepositories
    {
        List<Student> GetList();
    }
}
