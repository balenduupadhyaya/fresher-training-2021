﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models
{
    public class Employee
    {
        public int EmpId { get; set; }
        public string EmployeeName { get; set; }
        public int Mobile { get; set; }
    }
}
