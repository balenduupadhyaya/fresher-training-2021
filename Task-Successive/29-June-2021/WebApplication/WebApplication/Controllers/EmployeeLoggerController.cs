﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.Models;
using WebApplication.Services;

namespace WebApplication.Controllers
{
    public class EmployeeLoggerController : Controller
    {
        private readonly ILogger _logger;
        private readonly IMyLogger _mylogger;
        // GET: EmployeeLoggerController
        public EmployeeLoggerController(ILogger<EmployeeLoggerController> logger, IMyLogger mylogger)
        {
            _logger =logger;
            _mylogger = mylogger;
        }
        public IActionResult Index()
        {
            _mylogger.LogMessage();
            return View("Employee", new Employee());
        }
        public ActionResult Employee(Employee employee)
        {
        
            ViewBag.EmpId = employee.EmpId;
            ViewBag.EmployeeName = employee.EmployeeName;
            ViewBag.Mobile = employee.Mobile;
            return View("Employee", employee);
        }
        public IActionResult Privacy()
        {
            return View();
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
       public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier
    });
        }
    }
}
