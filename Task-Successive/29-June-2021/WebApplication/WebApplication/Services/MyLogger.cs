﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Services
{
    public class MyLogger : IMyLogger
    {
        public void LogMessage()
        {
            Debug.Write(string.Format("This is the message coming from custom logger"));
        }

        public void LogMessage(string message)
        {
            Debug.Write(string.Format("This is the message coming from custom logger" +message));
        }
    }
}
