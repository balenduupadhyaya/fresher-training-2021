﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Calculator.Models
{
    public class Calc
    {
        public int FirstNumber { get; set; }
        public int SecondNumber { get; set; }
        public int Total { get; set; }
    }
}
