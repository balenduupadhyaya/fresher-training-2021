﻿using Calculator.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Calculator.Controllers
{
    public class CalculatorController : Controller
        
    {
        private readonly ILogger<CalculatorController> _logger;
        public CalculatorController(ILogger<CalculatorController> logger)
        {
            _logger = logger;
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Calcc()
        {
            return View("Calcc", new Calc());

        }

        [HttpPost]

        public IActionResult Calcc(Calc c ,string cal)

        {
            if (cal == "add")
            {
                c.Total = c.FirstNumber + c.SecondNumber;
            }
            else if (cal == "sub")
            {
                c.Total = c.FirstNumber - c.SecondNumber;
                
            }
            
            else if (cal == "mul")
            {
                c.Total = c.FirstNumber * c.SecondNumber;
                
            }
            else
            {
                c.Total = c.FirstNumber / c.SecondNumber;
                
            }
            return View(c);
            
        }
    }
}
