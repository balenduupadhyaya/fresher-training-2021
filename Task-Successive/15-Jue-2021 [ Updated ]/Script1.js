function validate() {
    var firstname = document.regform.firstname.value;
    var lastname = document.regform.lastname.value;
    var rollno = document.regform.rollno.value;
    var email = document.regform.email.value;
    var pass = document.regform.password.value;
    var cpass = document.regform.confirmpassword.value;
    var addrs = document.regform.address.value;
    var mob = document.regform.mobilenumber.value;


    if (firstname == "") {
        document.getElementById('fnameerror').innerHTML = "Please fill FirstName";
        return false;
    }
    if (firstname.length <= 2 || firstname.length >= 40) {
        document.getElementById('fnameerror').innerHTML = "Lenght must be between 2 and 20";
        return false;
    }
    if (!isNaN(firstname)) {
        document.getElementById('fnameerror').innerHTML = "Only characters allowed";

        //single numeric value error
    }

    if (lastname == "") {
        document.getElementById('lname').innerHTML = "Please fill LastName";
        return false;
    }
    if (lastname.length <= 2 || lastname.length >= 40) {
        document.getElementById('lname').innerHTML = "Lenght must be between 2 and 20";
        return false;
    }
    if (!isNaN(lastname)) {
        document.getElementById('lname').innerHTML = "Only characters allowed";
        //single numeric value error
    }

    if (rollno == "") {
        document.getElementById('rno').innerHTML = "Please Fill RollNo";
        return false;
    }

    if (addrs == "") {
        document.getElementById('adds').innerHTML = "Address cant be empty";
        return false;
    }

    if (email == "") {
        document.getElementById('eml').innerHTML = "Email cannot be blank";
        return false;
    }
    if (!email.match("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$")) {
        document.getElementById('eml').innerHTML = "Not a valid email";
        return false;
    }

    if (pass.match("(?=.*[!@#%])(?=.*[a-z])(?=.*[A-Z]).{8,}")) {
        return true;
    }
    else {
        document.getElementById('pas').innerHTML = "Must contain at least one  number and one uppercase and lowercase letter and a special character , and at least 8 or more characters";
        return false;
    }
    if (cpass.match("(?=.*[!@#%])(?=.*[a-z])(?=.*[A-Z]).{8,}")) {
        return true;
    }
    else {
        document.getElementById('cpas').innerHTML = "Must contain at least one  number and one uppercase and lowercase letter and a special character , and at least 8 or more characters";
        return false;
    }
    if (pass != cpass) {
        document.getElementById('cpas').innerHTML = "Password not mached";
    }


    if (isNaN(mob)) {
        document.getElementById('mobile').innerHTML = "Cannot be characters";
        return false;
    }
    if (mob.length != 10) {
        document.getElementById('mobile').innerHTML = "Should be of 10 digit";
        return false;
    }
    //this.adddata();
    //return false;
}







    function adddata() {
        var firstname = document.regform.firstname.value;
        var middlename = document.regform.firstname.value;
        var lastname = document.regform.lastname.value;
        var rollno = document.regform.rollno.value;
        var email = document.regform.email.value;
        var pass = document.regform.password.value;
        var cpass = document.regform.confirmpassword.value;
        var addrs = document.regform.address.value;
        var mob = document.regform.mobilenumber.value;

        var table = document.getElementById("myTable");
        var row = table.insertRow(1);




        var c1 = row.insertCell(0);
        var c2 = row.insertCell(1);
        var c3 = row.insertCell(2);
        var c4 = row.insertCell(3);
        var c5 = row.insertCell(4);
        var c6 = row.insertCell(5);
        var c7 = row.insertCell(6);
        var c8 = row.insertCell(7);
        var c9 = row.insertCell(8);
        

        

        c1.innerHTML = firstname;
        c2.innerHTML = middlename;
        c3.innerHTML = lastname;
        c4.innerHTML = rollno;
        c5.innerHTML = email;
        c6.innerHTML = pass;
        c7.innerHTML = cpass;
        c8.innerHTML = addrs;
        c9.innerHTML = mob;


        





    }
