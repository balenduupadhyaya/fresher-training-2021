﻿

/*
      ------------------Method Overloading-----------------

*/



using System;

namespace Program_4
{
    class Program
    {
        static int MyMethod(int x)
        {
            return x;
        }
        static double MyMethod(double x)
        {
            return x;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Console.WriteLine(MyMethod(2));
            Console.WriteLine(MyMethod(2.25));

        }
    }
}
