﻿using System;

namespace Program_7
{
    class Program
    {
        public static void Show(params int[] val)
        {
            for(int i = 0; i < val.Length; i++)
            {
                Console.WriteLine(val[i]);
                Console.WriteLine();
            }
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Show(2, 3, 4, 5, 6);
            Show(2, 1, 2, 3, 4, 5, 6, 7, 8, 9);
        }
    }
}
