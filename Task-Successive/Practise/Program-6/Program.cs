﻿using System;

namespace Program_6
{
    class Program
    {
        public void Show(string name) // agar function static hai toh call krne ka lia object ki need nhi hai
        {
            Console.WriteLine("My name is " + name);
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Program P = new Program();
            P.Show("Rahul");
        }
    }
}
