﻿/*
 

Create a method which will take integer as nullable parameter.
Return "true" if the passed integer has some value in it and is not null
Return "false" otherwise

*/






using System;

namespace Program_5
{
    class Program
    {
        static void Null(int ? a)
        {
            if(a==null)
            {
                Console.WriteLine("Null hai bhai");
            }
            else
            {
                Console.WriteLine("Null nhi hai");
            }
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Console.WriteLine("Enter a");
            Null(Convert.ToInt32(Console.ReadLine()));     //We cant
            //int? a = 10;
           
            //if(a==null)
            //{
            //    Console.WriteLine("a contain null value");

            //}
            //else
            //{
            //    Console.WriteLine("a doesn't contain null value");
            //}
        }
    }
}
