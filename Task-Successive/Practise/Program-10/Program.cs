﻿using System;

namespace Program_10
{
    public class EnumExamples
    {
        public enum Seasons { Summer=12, Winter, Automn }

        public static void Main()
        {
            Console.WriteLine("Hello World!");
            int x = (int)Seasons.Summer;
            Console.WriteLine("Season = " + x);
        }
    }
}
