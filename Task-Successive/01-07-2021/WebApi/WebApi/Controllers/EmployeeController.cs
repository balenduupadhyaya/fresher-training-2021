﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using WebApplication.Models;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Consumes("application/json")]
    public class EmployeeController : Controller
    {
        //[ActionName("ind")] by using this we can use ind instead of index
        private List<EmployeeModel> employeeList = new List<EmployeeModel>();
        public EmployeeController()
        {
            employeeList.Add(new EmployeeModel
            {
                EmployeeId = 01,
                EmployeeName =" Balendu Upadhyaya",
                EmployeeAge = 21,
                Gender = "Male",
                MobileNumber = 9990037205
            }) ; 
            employeeList.Add(new EmployeeModel
            {
                EmployeeId = 02,
                EmployeeName ="Pawan Chaudhary",
                EmployeeAge = 21,
                Gender = "Male",
                MobileNumber = 9990037205
            }) ; 
            employeeList.Add(new EmployeeModel
            {
                EmployeeId = 03,
                EmployeeName ="Monika Trivedi",
                EmployeeAge = 24,
                Gender = "Female",
                MobileNumber = 9990037205
            }) ;
        }
        [HttpGet]
        [ProducesResponseType(typeof(EmployeeModel),(int)HttpStatusCode.Accepted)]
        public Task<EmployeeModel> GetEmployee()
        {
            var test = new EmployeeModel
            {
                EmployeeId = 00,
                EmployeeName = "TestPurpose",
                EmployeeAge = 000,
                Gender = "Tetsing",
                MobileNumber = 9990037205
            };
            return Task.FromResult(test);
        }

        [HttpGet]
        [Route("getall")]
        [ProducesResponseType(typeof(EmployeeModel), (int)HttpStatusCode.Created)]
        public Task<EmployeeModel> GetEmployee()
        {
            var test = new EmployeeModel
            {
                EmployeeId = 00,
                EmployeeName = "TestPurpose",
                EmployeeAge = 000,
                Gender = "Tetsing",
                MobileNumber = 9990037205
            };
            return Task.FromResult(test);
        }
        [HttpPost]
        [ApiVersion("3.0")]
        public Task<EmployeeModel> GetEmployee()
        {
            var test = new EmployeeModel
            {
                EmployeeId = 12,
                EmployeeName = "TestPurpose",
                EmployeeAge = 000,
                Gender = "Tetsing",
                MobileNumber = 9990037205
            };
            return Task.FromResult(test);

        }
        [HttpPost]
        [ApiVersion("3.0")]
       
    }
}
